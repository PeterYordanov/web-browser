function setLink(websitelink, text, separator)
{
	var link = websitelink;
	
	for(var i = 0; i < text.length; i++) {
			link += text[i];
			link += separator;
	}
	
	location.href = link;
}

function search()
{
	var search_option = document.getElementById("search-option");
	var search_text = document.getElementById("search-text");
	var search_button = document.getElementById("search-button");
	location.href.slice(0, location.href.length - 1);
        
	if(search_text.value == '') return;
	var text = search_text.value.split(" ");
		
	if(search_option.value == "google") {
		setLink("https://www.google.com/search?q=", text, '+');
	}
	
	if(search_option.value == "bing") {
		setLink("https://www.bing.com/search?q=", text, '+');
	}
	
	if(search_option.value == "yahoo") {
		setLink("https://search.yahoo.com/search?p=", text, '+');
	}
	
	if(search_option.value == "duckduckgo") {
		setLink("https://www.duckduckgo.com/", text, '%20');;
	}
}

function handleEnterPressed(event)
{
    if (event.keyCode === 0xd)
        document.getElementById("search-button").click();
}