var classcore_1_1synth_1_1_a_w_s_synthesizer =
[
    [ "~AWSSynthesizer", "classcore_1_1synth_1_1_a_w_s_synthesizer.html#af8f4b3031b8f22b5736d0d5eff5c2127", null ],
    [ "engine", "classcore_1_1synth_1_1_a_w_s_synthesizer.html#ad0a328dc97f66649d708a6e01ab6580c", null ],
    [ "saveToFile", "classcore_1_1synth_1_1_a_w_s_synthesizer.html#a116be805e27c91d5a977d067f8b68fea", null ],
    [ "setGender", "classcore_1_1synth_1_1_a_w_s_synthesizer.html#a2d121e417fb3c6e2087f5811797d9f23", null ],
    [ "setSampleRate", "classcore_1_1synth_1_1_a_w_s_synthesizer.html#a57b0298a53831e57b16a8ed618eb26c5", null ],
    [ "setVolume", "classcore_1_1synth_1_1_a_w_s_synthesizer.html#a4f6e04c00732e8e9b0fb7ffa5f2b43e8", null ],
    [ "synthesize", "classcore_1_1synth_1_1_a_w_s_synthesizer.html#ad36eb5b7ce09f32f5faae3f71f436c7a", null ]
];