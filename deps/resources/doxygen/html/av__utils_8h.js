var av__utils_8h =
[
    [ "AV_EMPTY_STRING", "av__utils_8h.html#a90448ef94cb8ba7258404ca18c69743b", null ],
    [ "AV_IS_GCC", "av__utils_8h.html#ad41e30fb9f1425a43a336f434c805c16", null ],
    [ "AV_IS_MSVC", "av__utils_8h.html#a0bfe8bf3ee6ec3d559eb4a910e0087e3", null ],
    [ "AV_IS_NIX", "av__utils_8h.html#a007b75d0085c33fc94afa26b2d306263", null ],
    [ "AV_IS_WINDOWS", "av__utils_8h.html#aba49f29d36b65a9ae37d2fd272a811f7", null ],
    [ "AV_RESOURCES_PATH_RELATIVE", "av__utils_8h.html#aadf1cd391de500c75a719e13a1e45500", null ],
    [ "AV_SAFE_FREE", "av__utils_8h.html#a4bd3a16d688cf16905e08c6d47b30b3f", null ],
    [ "AV_STR_EXPAND", "av__utils_8h.html#a94b64331c984731cb0cbe954c8e3f924", null ],
    [ "AV_TTS_DEFAULT_SAMPLE_RATE", "av__utils_8h.html#a94ec567bd9567d63953bca36d24f7df8", null ],
    [ "AV_TTS_DEFAULT_VOLUME", "av__utils_8h.html#ac79ce69aa1d0d788f632115b0e5259e0", null ],
    [ "AV_TTS_MAX_BUFFER_SIZE", "av__utils_8h.html#a48ac81e3ee5ad03802538624fb1bc70e", null ],
    [ "AV_UNUSED", "av__utils_8h.html#a01cfd8ba0c2e59b7e86c1cdccc257a4b", null ],
    [ "HIGH_NIBBLE", "av__utils_8h.html#a1e5cc31fbfd41b690ddc5fb4358544d6", null ],
    [ "LOW_NIBBLE", "av__utils_8h.html#a8ad691c7124ebf8dfa13c0e9373875ee", null ],
    [ "Q_TEST_OBJECT", "av__utils_8h.html#af0172e4712b568588a29d10bfbfee24d", null ],
    [ "AV_BOOL", "av__utils_8h.html#a3b59120dd4dd68079a5774af9893dc46", null ],
    [ "_bool", "av__utils_8h.html#ad35897bb5db9f645fbb47a071a4fb2f8", [
      [ "AV_FALSE", "av__utils_8h.html#ad35897bb5db9f645fbb47a071a4fb2f8a053feef1ed7db46ca3c6bb73adbfbecb", null ],
      [ "AV_TRUE", "av__utils_8h.html#ad35897bb5db9f645fbb47a071a4fb2f8a78236af6be3a51c601776a730bcc1d89", null ]
    ] ]
];