var classcore_1_1system_1_1_system_info_proxy =
[
    [ "SystemInfoProxy", "classcore_1_1system_1_1_system_info_proxy.html#aca1aec53d3d6378f21044d2e10886a6d", null ],
    [ "~SystemInfoProxy", "classcore_1_1system_1_1_system_info_proxy.html#adf428e344a6d6154b3cd2cefeb86f7c6", null ],
    [ "brandName", "classcore_1_1system_1_1_system_info_proxy.html#a4c16ee650264eb218b892346a178518e", null ],
    [ "cache", "classcore_1_1system_1_1_system_info_proxy.html#a95665ce6e1924e87942cf84bf2c4b80f", null ],
    [ "cores", "classcore_1_1system_1_1_system_info_proxy.html#ac9670c4c8df7686c458e3120bb2c3b3f", null ],
    [ "logicalCores", "classcore_1_1system_1_1_system_info_proxy.html#a36db57cf556c58c433e389c9c64f796e", null ],
    [ "pageSize", "classcore_1_1system_1_1_system_info_proxy.html#a5eef538a6d44771d4a51f36a0c6ab05c", null ],
    [ "RAMGigaBytes", "classcore_1_1system_1_1_system_info_proxy.html#a2bcc63ef773779d9dc42a5bd0becd48d", null ],
    [ "RAMKiloBytes", "classcore_1_1system_1_1_system_info_proxy.html#a0bbbac9fbd7869417ed67f0cac0d7cf4", null ],
    [ "vendorName", "classcore_1_1system_1_1_system_info_proxy.html#a44e8c812c40358124566eacb00710fbe", null ]
];