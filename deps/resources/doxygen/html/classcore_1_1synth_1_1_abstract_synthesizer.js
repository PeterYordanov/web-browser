var classcore_1_1synth_1_1_abstract_synthesizer =
[
    [ "Gender", "classcore_1_1synth_1_1_abstract_synthesizer.html#ae80cdcccf9eb3a59166d5ea15af61b81", null ],
    [ "Gender", "classcore_1_1synth_1_1_abstract_synthesizer.html#a6e903dfb124c1663d5181f7ed0d9ebf4", [
      [ "MALE", "classcore_1_1synth_1_1_abstract_synthesizer.html#a6e903dfb124c1663d5181f7ed0d9ebf4ae8c3bf5dd39a55afaaf6cfba346ec89b", null ],
      [ "FEMALE", "classcore_1_1synth_1_1_abstract_synthesizer.html#a6e903dfb124c1663d5181f7ed0d9ebf4a952d3528b741cf4ca6081a6f3bf3e040", null ],
      [ "UNKNOWN", "classcore_1_1synth_1_1_abstract_synthesizer.html#a6e903dfb124c1663d5181f7ed0d9ebf4a0322bd732164c32e510e6e3255e62edd", null ]
    ] ],
    [ "AbstractSynthesizer", "classcore_1_1synth_1_1_abstract_synthesizer.html#a005f5e208f47a2d6e3445ebe17978cba", null ],
    [ "~AbstractSynthesizer", "classcore_1_1synth_1_1_abstract_synthesizer.html#a4ca60822b1ef7bc67c61c09972c06043", null ],
    [ "engine", "classcore_1_1synth_1_1_abstract_synthesizer.html#a4fc341affb1b740f384e4d53b8fc93e2", null ],
    [ "saveToFile", "classcore_1_1synth_1_1_abstract_synthesizer.html#ae577dba544c19ecd7755817ae83224ed", null ],
    [ "setGender", "classcore_1_1synth_1_1_abstract_synthesizer.html#aeb06e1239d831c5670e49aba2c6e5ae1", null ],
    [ "setSampleRate", "classcore_1_1synth_1_1_abstract_synthesizer.html#a46bb555125ae9d343272f26796555211", null ],
    [ "setVolume", "classcore_1_1synth_1_1_abstract_synthesizer.html#a2015d780420d9dd67f9177a4895580c8", null ],
    [ "synthesize", "classcore_1_1synth_1_1_abstract_synthesizer.html#a222d0ecd1eff9d6c02fbc22980170682", null ]
];