var dir_df511e5bd85cec96854b39d5e1c27aa8 =
[
    [ "aver_algorithms.hpp", "aver__algorithms_8hpp.html", "aver__algorithms_8hpp" ],
    [ "aver_archive.hpp", "aver__archive_8hpp.html", [
      [ "Archive", "classcore_1_1archive_1_1_archive.html", "classcore_1_1archive_1_1_archive" ]
    ] ],
    [ "aver_automated_tests.hpp", "aver__automated__tests_8hpp.html", "aver__automated__tests_8hpp" ],
    [ "aver_ini_configuration.hpp", "aver__ini__configuration_8hpp_source.html", null ],
    [ "aver_noncopyable.hpp", "aver__noncopyable_8hpp_source.html", null ],
    [ "aver_nonmovable.hpp", "aver__nonmovable_8hpp_source.html", null ],
    [ "aver_synthesizer.hpp", "aver__synthesizer_8hpp.html", "aver__synthesizer_8hpp" ],
    [ "aver_system_info_proxy.hpp", "aver__system__info__proxy_8hpp.html", "aver__system__info__proxy_8hpp" ],
    [ "aver_textfile.hpp", "aver__textfile_8hpp.html", "aver__textfile_8hpp" ],
    [ "aver_thread.hpp", "aver__thread_8hpp.html", [
      [ "aver_thread", "classaver__thread.html", "classaver__thread" ]
    ] ],
    [ "aver_thread_pool.hpp", "aver__thread__pool_8hpp.html", [
      [ "aver_thread_pool", "classaver__thread__pool.html", "classaver__thread__pool" ]
    ] ]
];