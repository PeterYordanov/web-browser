var searchData=
[
  ['l1',['l1',['../structav__system__info.html#a42f28c45adcd55c3583c2bd859d56fe0',1,'av_system_info']]],
  ['l2',['l2',['../structav__system__info.html#a32f0bec3d951e47c4ee9c1177034f78c',1,'av_system_info']]],
  ['l3',['l3',['../structav__system__info.html#a8d9f61421957f366944c6837effe77cc',1,'av_system_info']]],
  ['level',['Level',['../av__debug_8h.html#a221b779e6bb7b8d40677d7642bfefac5',1,'Level():&#160;av_debug.h'],['../av__debug_8h.html#a247d1d02ad0817d3e3612c9d2495b4d0',1,'Level():&#160;av_debug.h']]],
  ['line_5fsize',['line_size',['../structav__cpu__cache.html#a7fc8be1fb79ada34e37aaceda733017b',1,'av_cpu_cache']]],
  ['logical_5fcores',['logical_cores',['../structav__system__info.html#ae443b679d1ec4bb516060b3b80a4e17d',1,'av_system_info']]],
  ['logicalcores',['logicalCores',['../classcore_1_1system_1_1_system_info_proxy.html#a36db57cf556c58c433e389c9c64f796e',1,'core::system::SystemInfoProxy']]],
  ['low_5fnibble',['LOW_NIBBLE',['../av__utils_8h.html#a8ad691c7124ebf8dfa13c0e9373875ee',1,'av_utils.h']]]
];
