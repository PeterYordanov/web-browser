var searchData=
[
  ['av_5fbool',['AV_BOOL',['../av__utils_8h.html#a3b59120dd4dd68079a5774af9893dc46',1,'av_utils.h']]],
  ['av_5fcpu_5fcache',['av_cpu_cache',['../av__system_8h.html#aefe0d74c8c85119b56358337f1f5faad',1,'av_system.h']]],
  ['av_5fcpu_5ffeatures',['av_cpu_features',['../av__system_8h.html#aed103c46ab7047764518f8cdcd0d4cb8',1,'av_system.h']]],
  ['av_5fcpu_5fstr_5fholder',['av_cpu_str_holder',['../av__system_8h.html#ae000783da46b9cee6ceb344d61f047b7',1,'av_system.h']]],
  ['av_5fregisters',['av_registers',['../av__system_8h.html#ae8eb7554a42f4e40416ccf8202e54e8e',1,'av_system.h']]],
  ['av_5fsystem_5finfo',['av_system_info',['../av__system_8h.html#ab226718aec28b47e03bbd46ce1d132f2',1,'av_system.h']]]
];
