var indexSectionsWithContent =
{
  0: "_abcdefghiklnpqrstuv",
  1: "acdiknqst",
  2: "a",
  3: "abceilprsv",
  4: "acelprs",
  5: "agl",
  6: "_gl",
  7: "fu",
  8: "ahl"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

