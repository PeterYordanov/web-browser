var av__debug_8h =
[
    [ "AV_DEBUG_COLOR_BLUE", "av__debug_8h.html#aed1eab0cf2cff16f09901bcdf39bee58", null ],
    [ "AV_DEBUG_COLOR_CYAN", "av__debug_8h.html#ae4db372b1b3008c481b0c1fadd9a87bf", null ],
    [ "AV_DEBUG_COLOR_GREEN", "av__debug_8h.html#a6475d2bcb46a3a5496d7904c2765bdd8", null ],
    [ "AV_DEBUG_COLOR_MAGENTA", "av__debug_8h.html#ade420270cc9e90d37e60e3347509612d", null ],
    [ "AV_DEBUG_COLOR_NORMAL", "av__debug_8h.html#abcf51a69472f992a04fe663aa1e1b1f0", null ],
    [ "AV_DEBUG_COLOR_RED", "av__debug_8h.html#a8e93c817b457f6ce1ccccbb2965f54d2", null ],
    [ "AV_DEBUG_COLOR_WHITE", "av__debug_8h.html#aec85df0877a0e116968afc0869b1cb4d", null ],
    [ "AV_DEBUG_COLOR_YELLOW", "av__debug_8h.html#aa0509f46928c2e75d730a87a39698864", null ],
    [ "Level", "av__debug_8h.html#a247d1d02ad0817d3e3612c9d2495b4d0", null ],
    [ "Level", "av__debug_8h.html#a221b779e6bb7b8d40677d7642bfefac5", [
      [ "AV_MESSAGE", "av__debug_8h.html#a221b779e6bb7b8d40677d7642bfefac5ab05db8e3a8088fed2e1102d288a811bc", null ],
      [ "AV_WARNING", "av__debug_8h.html#a221b779e6bb7b8d40677d7642bfefac5ad747835a46013ebc2f6b50383679eeee", null ],
      [ "AV_ERROR", "av__debug_8h.html#a221b779e6bb7b8d40677d7642bfefac5a76e93c92676c5cf44bfcf44a6b6c3aad", null ],
      [ "AV_DEFAULT", "av__debug_8h.html#a221b779e6bb7b8d40677d7642bfefac5a426f3858f815c34863def573d9aa2bdb", null ]
    ] ],
    [ "av_debug_level", "av__debug_8h.html#a6fa57a928eef1cb836dec497ad01bf84", null ],
    [ "av_debug_set_level", "av__debug_8h.html#a89616bfcfca03d27bf3cbdf9f64095f1", null ],
    [ "av_debugf", "av__debug_8h.html#a4d56f89d496daabb7d7bcdcaebc41a9c", null ],
    [ "av_printf", "av__debug_8h.html#a91a9aad708021e6d94583dc9d1e837bf", null ]
];