var classcore_1_1system_1_1_c_p_u_features_proxy =
[
    [ "CPUFeaturesProxy", "classcore_1_1system_1_1_c_p_u_features_proxy.html#af63ddbe4ed1d4aa070f32e7b5bc24315", null ],
    [ "~CPUFeaturesProxy", "classcore_1_1system_1_1_c_p_u_features_proxy.html#a271a2eeb96855a21c7f2aaf1c6bc091b", null ],
    [ "isAVX2Supported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#ac7f42699286f063204226172c35dd4ea", null ],
    [ "isAVXSupported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#aca31addbf7f11fe6568c7ab19d6451e8", null ],
    [ "isFPUSupported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#aebf9453d0f6c9eea02415ba1cdea5baf", null ],
    [ "isHyperThreadingSupported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#a9ad8f27d7f9ac0a0f8832e27ec7fe166", null ],
    [ "isMMXSupported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#a73a33e9032c8eeef82202e76c95062db", null ],
    [ "isPCLMULQDQSupported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#a0f5902614169af080d16e39d17983060", null ],
    [ "isSSE2Supported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#ac73d31ef57e79203f139580c215ca1d7", null ],
    [ "isSSE3Supported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#a29be50b0b1538466f55146f4aa86eae6", null ],
    [ "isSSE41Supported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#a6750d9b78321253c5db9150138f40af1", null ],
    [ "isSSE42Supported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#ac850df0d5b789e7c869ae2b7fe5f26c5", null ],
    [ "isSSESupported", "classcore_1_1system_1_1_c_p_u_features_proxy.html#a979b37896dd58575908094244ecd0bf1", null ]
];