var aver__algorithms_8hpp =
[
    [ "binarySearch", "aver__algorithms_8hpp.html#a765452c67c58e509c473431a159a61af", null ],
    [ "copy", "aver__algorithms_8hpp.html#a06f6de409e420d262b912115d70bb259", null ],
    [ "copyIf", "aver__algorithms_8hpp.html#a487884a000cb9fc6b71fdd16e4647ec4", null ],
    [ "equal", "aver__algorithms_8hpp.html#acf35f0c03d3cfff5be5316b44c86ead2", null ],
    [ "fill", "aver__algorithms_8hpp.html#a42c7d99f2f601303aee51e8eae664aec", null ],
    [ "nextPermutation", "aver__algorithms_8hpp.html#a84717ba1e3d1ab9440b867a7ac9be1cb", null ],
    [ "prevPermutation", "aver__algorithms_8hpp.html#a29a0101609f3d8ded8ef600bd023baf0", null ]
];