var hierarchy =
[
    [ "av_cpu_cache", "structav__cpu__cache.html", null ],
    [ "av_cpu_features", "structav__cpu__features.html", null ],
    [ "av_cpu_str_holder", "structav__cpu__str__holder.html", null ],
    [ "av_registers", "structav__registers.html", null ],
    [ "av_system_info", "structav__system__info.html", null ],
    [ "aver_thread", "classaver__thread.html", null ],
    [ "aver_thread_pool", "classaver__thread__pool.html", null ],
    [ "core::system::CPUFeaturesProxy", "classcore_1_1system_1_1_c_p_u_features_proxy.html", null ],
    [ "core::ini::INIParser", "classcore_1_1ini_1_1_i_n_i_parser.html", null ],
    [ "core::ini::INIWriter", "classcore_1_1ini_1_1_i_n_i_writer.html", null ],
    [ "core::ini::KeyValuePair", "structcore_1_1ini_1_1_key_value_pair.html", null ],
    [ "core::NonCopyable", "classcore_1_1_non_copyable.html", null ],
    [ "core::NonMovable", "classcore_1_1_non_movable.html", null ],
    [ "core::autotests::QAutomatedTestsManager", "classcore_1_1autotests_1_1_q_automated_tests_manager.html", null ],
    [ "QObject", null, [
      [ "core::archive::Archive", "classcore_1_1archive_1_1_archive.html", null ],
      [ "core::synth::AbstractSynthesizer", "classcore_1_1synth_1_1_abstract_synthesizer.html", [
        [ "core::synth::AWSSynthesizer", "classcore_1_1synth_1_1_a_w_s_synthesizer.html", null ],
        [ "core::synth::DefaultSynthesizer", "classcore_1_1synth_1_1_default_synthesizer.html", null ]
      ] ],
      [ "core::TextFileReader", "classcore_1_1_text_file_reader.html", null ],
      [ "core::TextFileWriter", "classcore_1_1_text_file_writer.html", null ]
    ] ],
    [ "core::ini::Section", "structcore_1_1ini_1_1_section.html", null ],
    [ "core::synth::SynthesizerFactory", "classcore_1_1synth_1_1_synthesizer_factory.html", null ],
    [ "core::system::SystemInfoProxy", "classcore_1_1system_1_1_system_info_proxy.html", null ]
];