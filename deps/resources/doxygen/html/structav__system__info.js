var structav__system__info =
[
    [ "cores", "structav__system__info.html#a1d105921d6971a570626cf9ef2fe2d0e", null ],
    [ "holder", "structav__system__info.html#a8f21bdacbd68f263efcc8fc051e327cc", null ],
    [ "l1", "structav__system__info.html#a42f28c45adcd55c3583c2bd859d56fe0", null ],
    [ "l2", "structav__system__info.html#a32f0bec3d951e47c4ee9c1177034f78c", null ],
    [ "l3", "structav__system__info.html#a8d9f61421957f366944c6837effe77cc", null ],
    [ "logical_cores", "structav__system__info.html#ae443b679d1ec4bb516060b3b80a4e17d", null ],
    [ "page_size", "structav__system__info.html#a14f985422d1f54d907c8ef0f4c7c21fc", null ],
    [ "ram_kilobytes", "structav__system__info.html#ae121e858b364e65437b7891ddc35c1ab", null ]
];