var annotated_dup =
[
    [ "core", null, [
      [ "archive", null, [
        [ "Archive", "classcore_1_1archive_1_1_archive.html", "classcore_1_1archive_1_1_archive" ]
      ] ],
      [ "autotests", null, [
        [ "QAutomatedTestsManager", "classcore_1_1autotests_1_1_q_automated_tests_manager.html", "classcore_1_1autotests_1_1_q_automated_tests_manager" ]
      ] ],
      [ "ini", null, [
        [ "INIParser", "classcore_1_1ini_1_1_i_n_i_parser.html", "classcore_1_1ini_1_1_i_n_i_parser" ],
        [ "INIWriter", "classcore_1_1ini_1_1_i_n_i_writer.html", "classcore_1_1ini_1_1_i_n_i_writer" ],
        [ "KeyValuePair", "structcore_1_1ini_1_1_key_value_pair.html", "structcore_1_1ini_1_1_key_value_pair" ],
        [ "Section", "structcore_1_1ini_1_1_section.html", "structcore_1_1ini_1_1_section" ]
      ] ],
      [ "synth", null, [
        [ "AbstractSynthesizer", "classcore_1_1synth_1_1_abstract_synthesizer.html", "classcore_1_1synth_1_1_abstract_synthesizer" ],
        [ "AWSSynthesizer", "classcore_1_1synth_1_1_a_w_s_synthesizer.html", "classcore_1_1synth_1_1_a_w_s_synthesizer" ],
        [ "DefaultSynthesizer", "classcore_1_1synth_1_1_default_synthesizer.html", "classcore_1_1synth_1_1_default_synthesizer" ],
        [ "SynthesizerFactory", "classcore_1_1synth_1_1_synthesizer_factory.html", "classcore_1_1synth_1_1_synthesizer_factory" ]
      ] ],
      [ "system", null, [
        [ "CPUFeaturesProxy", "classcore_1_1system_1_1_c_p_u_features_proxy.html", "classcore_1_1system_1_1_c_p_u_features_proxy" ],
        [ "SystemInfoProxy", "classcore_1_1system_1_1_system_info_proxy.html", "classcore_1_1system_1_1_system_info_proxy" ]
      ] ],
      [ "NonCopyable", "classcore_1_1_non_copyable.html", null ],
      [ "NonMovable", "classcore_1_1_non_movable.html", null ],
      [ "TextFileReader", "classcore_1_1_text_file_reader.html", "classcore_1_1_text_file_reader" ],
      [ "TextFileWriter", "classcore_1_1_text_file_writer.html", "classcore_1_1_text_file_writer" ]
    ] ],
    [ "av_cpu_cache", "structav__cpu__cache.html", "structav__cpu__cache" ],
    [ "av_cpu_features", "structav__cpu__features.html", "structav__cpu__features" ],
    [ "av_cpu_str_holder", "structav__cpu__str__holder.html", "structav__cpu__str__holder" ],
    [ "av_registers", "structav__registers.html", "structav__registers" ],
    [ "av_system_info", "structav__system__info.html", "structav__system__info" ],
    [ "aver_thread", "classaver__thread.html", "classaver__thread" ],
    [ "aver_thread_pool", "classaver__thread__pool.html", "classaver__thread__pool" ]
];