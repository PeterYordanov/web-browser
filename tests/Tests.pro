#include(../core/CoreAPI.pro)

HEADERS += \
    tests/iniparsertest.hpp \
    tests/archivetest.hpp \
    tests/systeminfotest.hpp \
    tests/textfiletest.hpp

SOURCES += \
    tests/iniparsertest.cpp \
    tests/archivetest.cpp \
    tests/systeminfotest.cpp \
    tests/textfiletest.cpp
