#ifndef SYSTEMINFOTEST_HPP
#define SYSTEMINFOTEST_HPP

#include <QtTest/QtTest>
#include "core/av_core.hpp"
#include AV_C_API_UTILITIES
#include AV_CPP_API_SYSTEM

class SystemInfoTest : public QObject
{
	Q_OBJECT
public:
	SystemInfoTest() = default;

AV_TEST_SLOTS:
	void testBrandName();
	void testVendor();
	void testRAMGB();
	void testPageSize();
	void testThreads();
	void testCores();

	void testCache1();
	void testCache2();
	void testCache3();

	void testHyperThreadingSupport();
	void testSSESupport();
	void testSSE2Support();
	void testSSE3Support();
	void testSSE41Support();
	void testSSE42Support();
	void testAVXSupport();
	void testAVX2Support();
	void testMMXSupport();
	void testFPUSupport();
	void testPCLMULQDQSupport();

private:
	core::system::CPUFeatures features;
	core::system::SystemInfo sysinfo;
};

#endif // SYSTEMINFOTEST_HPP
