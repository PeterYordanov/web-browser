#include "systeminfotest.hpp"
#include <QString>
#include "core/av_core.hpp"
#include AV_C_API_UTILITIES
#define AV_IS_EQUAL(x) x == AV_STR_EXPAND(Yes) ? true : false


void SystemInfoTest::testCache1()
{
	av_cpu_cache l1 = sysinfo.cache(1);
	QVERIFY(l1.level == 1);
	QVERIFY(l1.size == 32768);
	QVERIFY(l1.line_size == 64);
	QVERIFY(l1.associativity == 8);
}


void SystemInfoTest::testCache2()
{
	av_cpu_cache l2 = sysinfo.cache(2);
	QVERIFY(l2.level == 2);
	QVERIFY(l2.size == 262144);
	QVERIFY(l2.line_size == 64);
	QVERIFY(l2.associativity == 4);
}

void SystemInfoTest::testCache3()
{
	av_cpu_cache l3 = sysinfo.cache(3);
	QVERIFY(l3.level == 3);
	QVERIFY(l3.size == 6291456);
	QVERIFY(l3.line_size == 64);
	QVERIFY(l3.associativity == 12);
}


void SystemInfoTest::testBrandName()
{
	QVERIFY(sysinfo.brandName() == "Intel(R) Core(TM) i7-7700HQ CPU @ 2.80GHz");
}

void SystemInfoTest::testVendor()
{
	QVERIFY(sysinfo.vendorName() == "GenuineIntel");
}

void SystemInfoTest::testRAMGB()
{
	QVERIFY(sysinfo.RAMGigaBytes() == 16);
}

void SystemInfoTest::testPageSize()
{
	QVERIFY(sysinfo.pageSize() == 4096);
}

void SystemInfoTest::testThreads()
{
	QVERIFY(sysinfo.logicalCores() == 8);
}

void SystemInfoTest::testCores()
{
	QVERIFY(sysinfo.cores() == 4);
}

void SystemInfoTest::testHyperThreadingSupport()
{
	QVERIFY(AV_IS_EQUAL(features.isHyperThreadingSupported()));
}

void SystemInfoTest::testSSESupport()
{
	QVERIFY(AV_IS_EQUAL(features.isSSESupported()));
}

void SystemInfoTest::testSSE2Support()
{
	QVERIFY(AV_IS_EQUAL(features.isSSE2Supported()));
}

void SystemInfoTest::testSSE3Support()
{
	QVERIFY(AV_IS_EQUAL(features.isSSE3Supported()));
}

void SystemInfoTest::testSSE41Support()
{
	QVERIFY(AV_IS_EQUAL(features.isSSE41Supported()));
}

void SystemInfoTest::testSSE42Support()
{
	QVERIFY(AV_IS_EQUAL(features.isSSE42Supported()));
}

void SystemInfoTest::testAVXSupport()
{
	QVERIFY(AV_IS_EQUAL(features.isAVXSupported()));
}

void SystemInfoTest::testAVX2Support()
{
	QVERIFY(AV_IS_EQUAL(features.isAVX2Supported()));
}

void SystemInfoTest::testMMXSupport()
{
	QVERIFY(AV_IS_EQUAL(features.isMMXSupported()));
}

void SystemInfoTest::testFPUSupport()
{
	QVERIFY(AV_IS_EQUAL(features.isFPUSupported()));
}

void SystemInfoTest::testPCLMULQDQSupport()
{
	QVERIFY(AV_IS_EQUAL(features.isPCLMULQDQSupported()));
}
