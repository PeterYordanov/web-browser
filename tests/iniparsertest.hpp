#ifndef INIPARSERTEST_HPP
#define INIPARSERTEST_HPP

#include <QObject>
#include "core/av_core.hpp"
#include AV_CPP_API_INI

class INIParserTest : public QObject
{
	Q_OBJECT
public:
	INIParserTest();

AV_TEST_SLOTS:
	void testINIWrite();
	void testINIRead();
};

#endif // INIPARSERTEST_HPP
