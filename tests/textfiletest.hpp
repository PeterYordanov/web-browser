#ifndef TEXTFILEREADERTEST_HPP
#define TEXTFILEREADERTEST_HPP

#include <QtTest/QtTest>
#include "core/av_core.hpp"
#include AV_C_API_UTILITIES
#include AV_CPP_API_TEXT_FILE

class TextFileTest : public QObject
{
	Q_OBJECT
public:
	TextFileTest() = default;

AV_TEST_SLOTS:
	void testWriteFile();
	void testReadFile();

private:
	core::TextFileReader reader;
	core::TextFileWriter writer;
};



#endif // TEXTFILEREADERTEST_HPP
