#include "textfiletest.hpp"
#include <QFile>

#define AV_TEST_TEXTFILE AV_STR_EXPAND(../aver/tests/files/testtxtfile.txt)

void TextFileTest::testWriteFile()
{
	writer.setPath(toAbsolutePath(AV_TEST_TEXTFILE));
	writer.setText(QStringList() << "just" << "some" << "text");
	writer.write();

	QFile file(toAbsolutePath(AV_TEST_TEXTFILE));
	QVERIFY(file.exists());
}

void TextFileTest::testReadFile()
{
	reader.setPath(toAbsolutePath(AV_TEST_TEXTFILE));
	QStringList text = reader.read();
	QVERIFY(!text.isEmpty());
	QVERIFY(text[2] == "text");
}

