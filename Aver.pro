QT += core
QT += gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += webenginewidgets
QT += webengine
#QT += webkitwidgets - Not supported anymore

TARGET = AVER
TEMPLATE = app
#RESOURCES = res.qrc

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler)
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000 # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17
CONFIG -= exceptions
unix : { CONFIG -= old-style-cast }
CONFIG += Og

#unix: CONFIG += -g -rdynamic
unix: QMAKE_LFLAGS += -g -rdynamic

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

message($$TARGET :)
message(Qt version: $$[QT_VERSION])
message(Qt components: $$QT)
win32 | win64 { message(Current platform: Windows (32/64-bit)) }
unix { message(Current platform: Unix (or Unix-like)) }
message(----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------)
EVERYTHING = $$SOURCES $$HEADERS
message(Files : $$EVERYTHING)
message(Binary files: $$[QT_INSTALL_BINS])
message(Header files: $$[QT_INSTALL_HEADERS])
message(Libraries: $$[QT_INSTALL_LIBS])
message(Data files: $$[QT_INSTALL_DATA])
message(----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------)
message(Configurations: $$CONFIG)
message(----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------)

include(core/CoreAPI.pro)
#include(tests/Tests.pro)

HEADERS += \
    aver/aver_dialogs.hpp \
    aver/aver_download_manager.hpp \
    aver/aver.hpp \
    aver/aver_bookmarks.hpp \
    aver/aver_webengine.hpp \
    aver/aver_tab_widget.hpp

SOURCES += \
    main.cpp \
    aver/aver_dialogs.cpp \
    aver/aver_download_manager.cpp \
    aver/aver.cpp \
    aver/aver_bookmarks.cpp \
    aver/aver_webengine.cpp \
    aver/aver_tab_widget.cpp




# ****************************************flite (Linux)********************************
unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_time_awb

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_time_awb.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_us_awb

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_us_awb.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_us_kal

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_us_kal.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_us_kal16

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_us_kal16.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_us_slt

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_us_slt.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_us_rms

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_us_rms.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_usenglish

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_usenglish.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_grapheme_lang

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_grapheme_lang.a

unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_grapheme_lex

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_grapheme_lex.a


unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_indic_lang

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_indic_lang.a


unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmu_indic_lex

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmu_indic_lex.a


unix: LIBS += -L$$PWD/deps/flite/build/x86_64-linux-gnu/lib/ -lflite_cmulex

INCLUDEPATH += $$PWD/deps/flite/include
DEPENDPATH += $$PWD/deps/flite/include

unix: PRE_TARGETDEPS += $$PWD/deps/flite/build/x86_64-linux-gnu/lib/libflite_cmulex.a

# ****************************libarchive(Windows)************************************

#win32: LIBS += -L$$PWD/deps/libarchive_x64-windows/debug/lib/ -larchive
#win32: INCLUDEPATH += $$PWD/deps/libarchive_x64-windows/include
#win32: DEPENDPATH += $$PWD/deps/libarchive_x64-windows/include

# ********************************pthreads(Windows)************************************
win32: LIBS += -L$$PWD/deps/pthreads_x64-windows/debug/lib/ -lpthreadsVC2d
win32: INCLUDEPATH += $$PWD/deps/pthreads_x64-windows/include
win32: DEPENDPATH += $$PWD/deps/pthreads_x64-windows/include

# ********************************pthreads(Linux)************************************
unix: LIBS += -lpthread

# ****************************libarchive(Linux)************************************
#unix: LIBS += -larchive
