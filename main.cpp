#define AVER_START_NORMAL 0
#define AVER_START_AUTOMATED_TESTS 1
#include <QApplication>
#include "aver/aver.hpp"
#include "core/av_core.hpp"
#include AV_C_API_SIGNAL_HANDLER
#include AV_C_API_SYSTEM
#include AV_C_API_UTILITIES
#include AV_CPP_API_AUTOMATED_TESTING
#include AV_CPP_API_INI

int main(int argc, char* argv[])
{
	av_register_signals();
	QApplication app(argc, argv);
	MainWindow w;
	w.show();
	return app.exec();
}
