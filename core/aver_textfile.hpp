#ifndef AVER_TEXTFILEREADER_HPP
#define AVER_TEXTFILEREADER_HPP


/**
 * @file aver_textfile.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

#include <QObject>
#include "core/av_core.hpp"
#include AV_C_API_UTILITIES

#ifdef AV_IS_WINDOWS
# define NEW_LINE QString("\r\n")
#else
# define NEW_LINE QString("\n")
#endif

namespace core {

	/* A component taken from an old project : */
	class TextFileReader : public QObject
	{
		Q_OBJECT

	public:
		explicit TextFileReader(QObject* parent = nullptr);
		~TextFileReader() = default;

	public:
		QStringList read();
		QString readAtLine(const unsigned int);
		QStringList readUntilLine(const unsigned int);
		void setPath(const QString&);

		inline QStringList content() const
		{ return m_content; }

		inline QString filePath() const
		{ return m_filePath; }

	private:
		unsigned int m_lines;
		QStringList m_content;
		QString	m_filePath;
	};

	/* A component taken from an old project : */
	class TextFileWriter : public QObject
	{
		Q_OBJECT

	public:
		explicit TextFileWriter(QObject* parent = nullptr);
		~TextFileWriter() = default;

	public:
		void write();
		void writeUntilLine(const unsigned int);
		void writeAtLine(const unsigned int);
		void setPath(const QString&);
		void setText(const QStringList&);

	private:
		QStringList m_content;
		unsigned int m_lines;
		QString m_filePath;
	};
}

#endif // AVER_TEXTFILEREADER_HPP
