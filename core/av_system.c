#include "av_system.h"
#include <memory.h>

void av_cpuid(av_registers* regs, unsigned int level)
{
#if AV_IS_WINDOWS
	//oops, that cast tho
	__cpuid((int*)regs, (int)level);
#else
	__get_cpuid(level, &regs->eax,
					&regs->ebx,
					&regs->ecx,
					&regs->edx);
#endif
}

av_cpu_str_holder av_system_get_cpu_brand_name(av_system_info* _this)
{
	av_registers cpu;
	char brand[AV_CPU_BRAND_LENGTH];
	memset(brand, 0, sizeof(brand));
	av_cpuid(&cpu, 0x80000002);
	memcpy(brand, &cpu, sizeof(cpu));
	av_cpuid(&cpu, 0x80000003);
	memcpy(brand + 16, &cpu, sizeof(cpu));
	av_cpuid(&cpu, 0x80000004);
	memcpy(brand + 32, &cpu, sizeof(cpu));

	strcpy(_this->holder.brand, brand);

	return _this->holder;
}

#if AV_IS_WINDOWS

void av_system_info_init(av_system_info* _this)
{
	unsigned long long RAM;
	GetPhysicallyInstalledSystemMemory(&RAM);
	_this->ram_kilobytes = RAM;

	SYSTEM_INFO info;
	GetSystemInfo(&info);
	_this->logical_cores = (int)info.dwNumberOfProcessors;
	_this->page_size = (int)info.dwPageSize;

	typedef DWORD (WINAPI* LPFN_GLPI)(PSYSTEM_LOGICAL_PROCESSOR_INFORMATION, PDWORD);

	PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = NULL;
	PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = NULL;
	DWORD ret = 0;
	DWORD byte_offset = 0;
	LPFN_GLPI glpi = (LPFN_GLPI)GetProcAddress(GetModuleHandle(L"kernel32"), "GetLogicalProcessorInformation");
	AV_BOOL done = AV_FALSE;
	int cores = 0;

	while (!done) {
		DWORD rc = glpi(buffer, &ret);

		if (!rc) {
			if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
				AV_SAFE_FREE(buffer);

				buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)malloc(ret);
			}
		} else done = AV_TRUE;
	}

	ptr = buffer;

	while (byte_offset + sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) <= ret) {

		switch(ptr->Relationship)
		{
		case RelationProcessorCore:
			cores++;
			break;
		case RelationCache:
			PCACHE_DESCRIPTOR cache = &ptr->Cache;
			if (cache->Level == 1) {
				_this->l1.level = cache->Level;
				_this->l1.size = (int)cache->Size;
				_this->l1.line_size = cache->LineSize;
				_this->l1.associativity = cache->Associativity;
			}
			else if (cache->Level == 2){
				_this->l2.level = cache->Level;
				_this->l2.size = (int)cache->Size;
				_this->l2.line_size = cache->LineSize;
				_this->l2.associativity = cache->Associativity;
			}
			else if (cache->Level == 3) {
				_this->l3.level = cache->Level;
				_this->l3.size = (int)cache->Size;
				_this->l3.line_size = cache->LineSize;
				_this->l3.associativity = cache->Associativity;
			}
			break;
		}

		byte_offset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
		ptr++;
	}

	_this->cores = cores;
	AV_SAFE_FREE(buffer);
}

unsigned long long av_system_get_ram_kb(av_system_info* _this)
{
	return _this->ram_kilobytes;
}

int av_system_get_ram_gb(av_system_info* _this)
{
	unsigned long long memory = _this->ram_kilobytes;
	return (int)floor(memory * pow(10, -6));
}

int av_system_get_page_size(av_system_info* _this)
{
	return _this->page_size;
}

int av_system_get_logical_cores(av_system_info* _this)
{
	return _this->logical_cores;
}

int av_system_get_cores(av_system_info* _this)
{
	return _this->cores;
}

#endif // AV_IS_WINDOWS

#if AV_IS_NIX
#include <unistd.h>
#include <sys/sysinfo.h>
#include <linux/sched.h>
#include <stdlib.h>
#include "av_debug.h"

AV_BOOL av_starts_with(const char* str, const char* startswith, int size)
{
	for(int i = 0; i < size; i++) {
		if(*str++ != *startswith++)
			return AV_FALSE;
	}

	return AV_TRUE;
}

void av_system_info_init(av_system_info* _this)
{
	_this->logical_cores = (int)sysconf(_SC_NPROCESSORS_CONF);
	_this->page_size = (int)sysconf(_SC_PAGESIZE);
	struct sysinfo info;
	sysinfo(&info);
	_this->ram_kilobytes = info.totalram;
	_this->l1.level = 1;
	_this->l1.size = (int)sysconf(_SC_LEVEL1_DCACHE_SIZE);
	_this->l1.line_size = (int)sysconf(_SC_LEVEL1_DCACHE_LINESIZE);
	_this->l1.associativity = (int)sysconf(_SC_LEVEL1_DCACHE_ASSOC);
	_this->l2.level = 2;
	_this->l2.size = (int)sysconf(_SC_LEVEL2_CACHE_SIZE);
	_this->l2.line_size = (int)sysconf(_SC_LEVEL2_CACHE_LINESIZE);
	_this->l2.associativity = (int)sysconf(_SC_LEVEL2_CACHE_ASSOC);
	_this->l3.level = 3;
	_this->l3.size = (int)sysconf(_SC_LEVEL3_CACHE_SIZE);
	_this->l3.line_size = (int)sysconf(_SC_LEVEL3_CACHE_LINESIZE);
	_this->l3.associativity = (int)sysconf(_SC_LEVEL3_CACHE_ASSOC);

	//A little hacky, but still...
	FILE* cmd = popen("grep '^cpu cores' /proc/cpuinfo | head -n 1 | tail -c 2", "r");
	char buffer[sizeof(int)];
	fread(buffer, 1, sizeof(buffer), cmd);
	sscanf(buffer, "%u", &_this->cores);
	pclose(cmd);
}

int av_system_get_cores(av_system_info* _this)
{
	return _this->cores;
}

int av_system_get_logical_cores(av_system_info* _this)
{
	return _this->logical_cores;
}

int av_system_get_page_size(av_system_info* _this)
{
	return _this->page_size;
}

unsigned long long av_system_get_ram_kb(av_system_info* _this)
{
	return _this->ram_kilobytes;
}

int av_system_get_ram_gb(av_system_info* _this)
{
	unsigned long long memory = _this->ram_kilobytes;
	return (int)floor(memory * pow(10, -9));
}

#endif // AV_IS_NIX

av_cpu_cache av_system_get_cache_info(av_system_info* _this, int level)
{
	if(level == 1) return _this->l1;
	else if(level == 2) return _this->l2;
	else if(level == 3) return _this->l3;
	av_cpu_cache empty = { .level = 0, .associativity = 0, .line_size = 0 };
	return empty;
}

av_cpu_str_holder av_system_get_vendor(av_system_info* _this)
{
	av_registers regs;
	av_cpuid(&regs, 0);
	uint32_t vendor[AV_CPU_REGISTERS];
	memset(vendor, 0, sizeof(vendor));
	vendor[0] = regs.ebx;
	vendor[1] = regs.edx;
	vendor[2] = regs.ecx;

	strcpy(_this->holder.vendor, vendor);
	return _this->holder;
}

void av_system_info_uninit(av_system_info* _this)
{
	(void)_this;
}

/***********************************************************************/

void av_cpu_features_init(av_cpu_features* _this)
{
	av_cpuid(&_this->regs, 1);
}

#define AV_IS_SUPPORTED(x) x ? AV_STR_EXPAND(Yes) : AV_STR_EXPAND(No)
#define AV_IS_SUPPORTED_TO_STR(x)

const char* av_cpu_is_hyper_threading_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.edx & 0x1000000);
}

const char* av_cpu_is_sse_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.edx & 0x02000000);
}

const char* av_cpu_is_sse2_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.edx & 0x04000000);
}

const char* av_cpu_is_sse3_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.ecx & 0x0000001);
}

const char* av_cpu_is_sse41_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.ecx & 0x0008000);
}

const char* av_cpu_is_sse42_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.ecx & 0x00100000);
}

const char* av_cpu_is_avx_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.ecx & 0x1000000);
}

const char* av_cpu_is_avx2_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.ecx & 0x00000020);
}

const char* av_cpu_is_mmx_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.edx & 0x00000017);
}

const char* av_cpu_is_fpu_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.edx & 0x0000001);
}

const char* av_cpu_is_pclmulqdq_supported(av_cpu_features* _this)
{
	return AV_IS_SUPPORTED(_this->regs.ecx & 0x0000002);
}
