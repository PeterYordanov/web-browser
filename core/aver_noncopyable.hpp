#ifndef AVER_NONCOPYABLE_HPP
#define AVER_NONCOPYABLE_HPP

/**
 * @file aver_noncopy_nonmove.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

namespace core {
	
	/**
	 * @brief Any class that inherits from it will not be able to be copied
	 */
	class NonCopyable
	{
	public:
	#if __cplusplus >= 201103L
		NonCopyable() = default;
		NonCopyable(const NonCopyable&) = delete;
		NonCopyable& operator=(const NonCopyable&) = delete;
	#else
	private:
		inline NonCopyable(){}
		NonCopyable(const NonCopyable&);
		NonCopyable& operator=(const NonCopyable&);
	#endif
	};
}

#endif // AVER_NONCOPYABLE_HPP
