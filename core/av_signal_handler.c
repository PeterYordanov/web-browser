#include "av_signal_handler.h"
#include "av_debug.h"
#include "av_utils.h"

#if AV_IS_WINDOWS
#include <memory.h>
#include <Windows.h>
#include <ImageHlp.h>
#include <stdio.h>
#pragma comment(lib, "Dbghelp.lib")
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "Kernel32.lib")

void av_print_stack_trace(void)
{
	void* stack[AV_MAX_STACK_SIZE];
	unsigned short frames;
	SYMBOL_INFO* symbol;
	HANDLE process = GetCurrentProcess();
	FILE* debug_file = fopen(AV_SIGNAL_DEBUG_FILE_PATH, AV_SIGNAL_DEBUG_FILE_WRITE);

	SymInitialize(process, NULL, TRUE);
	frames = CaptureStackBackTrace(0, 100, stack, NULL);
	symbol = (SYMBOL_INFO*)calloc(sizeof(SYMBOL_INFO) + 256 * sizeof(char), 1);
	symbol->MaxNameLen = AV_MAX_SYMBOL_NAME_LENGTH;
	symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

	for(int i = 0; i < frames; i++) {
		SymFromAddr(process, (DWORD64)(stack[i]), NULL, symbol);
		av_debug_set_level(AV_ERROR);

		unsigned int frame_number = frames - i - 1;
		const char* format = "(%i) (0x%0X - %s)\n";
		av_debugf(format, frame_number, symbol->Address, symbol->Name);
		fprintf(debug_file, format, frame_number, symbol->Address, symbol->Name);
	}

	av_debug_set_level(AV_DEFAULT);

	fclose(debug_file);
	AV_SAFE_FREE(symbol);
}

#endif

#if AV_IS_NIX

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/stat.h>

void av_print_stack_trace(void)
{
	struct sigcontext context;

#	if defined(__i386__)
	void* caller_address = (void*)context.eip; // EIP : x86-specific
#	elif defined(__x86_64__)
	void* caller_address = (void*)context.rip; // RIP : x86_64-specific
#	endif

	void* stack[AV_MAX_STACK_SIZE];
	int size = backtrace(stack, AV_MAX_SYMBOL_NAME_LENGTH);
	stack[1] = caller_address;
	char** messages = backtrace_symbols(stack, size);

	FILE* debug_file = fopen(AV_SIGNAL_DEBUG_FILE_PATH, AV_SIGNAL_DEBUG_FILE_WRITE);

	const char* format = "(%d) %s\n";
	for (int i = 0; i < size; ++i) {
		av_debug_set_level(AV_ERROR);
		av_debugf(format, i, messages[i]);
		fprintf(debug_file, format, i, messages[i]);
	}

	fclose(debug_file);
	AV_SAFE_FREE(messages);
}

#endif // AV_IS_NIX

AV_NORETURN void av_signal_callback(int signum)
{
	av_debug_set_level(AV_ERROR);
	av_debugf("Caught signal : %s\n", _av_fetch_signal_name(signum));
	av_print_stack_trace();
	exit(signum);
}

void av_register_signals(void)
{
#	define AV_SIGNALS_HANDLED 6

	int sigs[AV_SIGNALS_HANDLED] = {
		SIGINT, //2, Program interrupts (ctrl+c)
		SIGILL, //4, Illegal instruction
		SIGABRT, //22, Abort (caused by assertions or abort() )
		SIGFPE, /* 8, Floating-point arithmetic exception
				(includes division by zero and overflow) */
		SIGSEGV, //11, Segmentation fault
		SIGTERM //15, Termination request
	};

	for(int i = 0; i <= AV_SIGNALS_HANDLED; i++)
		signal(sigs[i], av_signal_callback);
}

const char* _av_fetch_signal_name(int signum)
{
	switch(signum)
	{
		case AV_SIGNAL_PROGRAM_INTERRUPT:
			return "Program Interrupts";
		case AV_SIGNAL_ILLEGAL_INSTRUCTION:
			return "Illegal Instruction";
		case AV_SIGNAL_ABORT:
			return "Signal Abort";
		case AV_SIGNAL_FP_EXCEPTION:
			return "Floating Point Exception (Division by zero/overflow)";
		case AV_SIGNAL_SEG_FAULT:
			return "Segmentation fault";
		case AV_SIGNAL_TERMINATION:
			return "Termination request";
		default:
			return "Unrecognized signal (not handled)";
	}
}
