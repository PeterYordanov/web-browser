#ifndef Archive_HPP
#define Archive_HPP

/**
 * @file aver_archive.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

//#include "abstractarchive.hpp"
#include <QString>
#include <QObject>

extern "C" {
#   include <archive.h>
#   include <archive_entry.h>
}

namespace core::archive
{
	/* A component taken from an old project : */
	/**
	 * @brief Class that represents archive documents
	 */
	class Archive : public QObject
	{
		Q_OBJECT
	public:
		typedef enum TypeData : int {
			FileData,
			HeaderData
		} TypeOfArchiveData;

	public:
		/**
		 * @param archive Path to the archive, including its name
		 * @param parent QObject parent to take care of its cleaning
		 */
		explicit Archive(const QString& archive, QObject* parent = Q_NULLPTR);
		~Archive();

		/**
		 * @brief Sets the type of data to extract (all data or header data only)
		 * @param type Type of data
		 */
		inline void setExtractionMethod(TypeOfArchiveData type)
		{
			m_type = type;
		}

		/**
		 * @brief Extracts to a specified folder
		 * @param p Where we should extract to
		 */
		void extractTo(const QString& p);

		/**
		 * @brief Path of the archive
		 * @return ''
		 */
		inline QString archivePath() const
		{ return m_archivePath; }


		/**
		 * @brief Path of the folder where the archive was extracted
		 * @return ''
		 */
		inline QString extractToPath() const
		{ return m_extractToPath; }


		inline struct archive* archive() const
		{ return m_archive; }

		auto isOpened() const -> bool
		{ return m_isOpened; }


	private:
		struct archive* m_archive;
		struct archive* m_disk;
		QString			m_archivePath;
		QString			m_extractToPath;
		bool			m_isOpened = false;
		TypeOfArchiveData m_type;
	};

}

#endif
