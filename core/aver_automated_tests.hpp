#ifndef AVER_AUTOMATED_TESTS_HPP
#define AVER_AUTOMATED_TESTS_HPP

/**
 * @file aver_automated_tests.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief Utility tools for testing
 *
 * @see .html
 * @see .html
 */

//#include <QObject>
#include <QTest>
#include <QtGlobal>

QT_BEGIN_NAMESPACE
class QObject;
template<typename T>
class QList;
QT_END_NAMESPACE
namespace core::autotests
{
	/**
	 * @brief Manager class that extends QTest by allowing you to register and execute multiple tests.
	 */
	class QAutomatedTestsManager
	{
	public:

		/**
		 * @brief Adds a test to a list, in order to be executed when run is called
		 * @param o Test to add
		 */
		void addTest(QObject* o);

		/**
		 * @brief Removes a test from the list
		 * @param o Test to remove
		 */
		void removeTest(QObject* o);

		/**
		 * @brief Runs all tests
		 * @param argc Argument count
		 * @param argv Argument string array
		 */
		int run(int argc, char* argv[]);
		bool contains(const QObject* const obj)
		{ return m_testsList.contains(const_cast<QObject* const>(obj)); }

		inline QList<QObject*> testsList() const
		{ return m_testsList; }

	private:
		QList<QObject*> m_testsList;
	};


	/**
	 * @brief Sets up everything for testing
	 */
#	define AV_TEST_MAIN_BEGIN \
			QCoreApplication app(argc, argv); \
			app.setAttribute(Qt::AA_Use96Dpi, true); \
			QTEST_DISABLE_KEYPAD_NAVIGATION \
			QTEST_SET_MAIN_SOURCE_PATH \
			core::autotests::QAutomatedTestsManager manager;

	/**
	 * @brief Adds a test
	 * @param name Class name of the test
	 */
#	define AV_REGISTER_TEST(name) \
			name test##name; \
			manager.addTest(&test##name);

	/**
	 * @brief Removes a test
	 * @param name Class name of the test
	 */
#	define AV_UNREGISTER_TEST(name) \
			name test##name; \
			manager.removeTest(&test##name);

	/**
	 * @brief Executes all tests and returns exit code
	 */
#	define AV_TEST_MAIN_END manager.run(argc, argv)

	inline QString testFilesFolderPath()
	{
		QDir dir("../../aver/tests/testfiles/");
		return dir.absolutePath();
	}
}

#endif // AVER_AUTOMATED_TESTS_HPP
