QT += testlib

# < C++ >
HEADERS += \
    core/av_core.hpp \
    aver_noncopyable.hpp \
    core/aver_nonmovable.hpp \
    core/aver_system_info_proxy.hpp \
    core/aver_automated_tests.hpp \
    core/aver_algorithms.hpp \
    #core/aver_archive.hpp \
    core/aver_thread.hpp \
    core/aver_thread_pool.hpp \
    core/aver_synthesizer.hpp \
    core/aver_textfile.hpp \
    core/aver_ini_configuration.hpp

SOURCES += \
    core/aver_nonmovable.cpp \
    core/aver_noncopyable.cpp \
    core/aver_system_info_proxy.cpp \
    core/aver_automated_tests.cpp \
    core/aver_algorithms.cpp \
    #core/aver_archive.cpp \
    core/aver_thread.cpp \
    core/aver_thread_pool.cpp \
    core/aver_synthesizer.cpp \
    core/aver_textfile.cpp \
    core/aver_ini_configuration.cpp

# < C >
HEADERS += \
    core/av_utils.h \
    core/av_debug.h \
    core/av_signal_handler.h \
    core/av_system.h

SOURCES += \
    core/av_utils.c \
    core/av_debug.c \
    core/av_signal_handler.c \
    core/av_system.c
