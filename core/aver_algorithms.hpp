#ifndef AVER_ALGORITHMS_HPP
#define AVER_ALGORITHMS_HPP

/**
 * @file aver_algorithms.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief A wrapper, zero-cost abstraction around std algorithms
 *
 * @see .html
 * @see .html
 */

#include <algorithm>
#include <string>

namespace core::algorithms
{
	template<typename T, typename U>
	void transform(T& container, const U& func)
	{
		std::transform(container.begin(), container.end(), func);
	}

	template<typename T>
	bool nextPermutation(T t)
	{
		return std::next_permutation(t.begin(), t.end());
	}

	template<typename T>
	bool prevPermutation(T t)
	{
		return std::prev_permutation(t.begin(), t.end());
	}

	template<typename T, typename U, typename V>
	void copyIf(T src, U dest, V pred)
	{
		std::copy_if(src.begin(), src.end(),
					 dest.begin(), pred);
	}

	template<typename T, typename U>
	void copy(T src, U dest)
	{
		std::copy(src.begin(), src.end(), dest.begin());
	}

	template<typename T,
			 typename U = int>
	void fill(T t, U val = 0)
	{
		std::fill(t.begin(), t.end(), val);
	}

	template<typename T, typename U>
	bool equal(T f, U s)
	{
		return std::equal(f.begin(), f.end(), s.begin());
	}

	template<typename T,
			 typename U = int>
	void binarySearch(T f, U val)
	{
		std::binary_search(f.begin(), f.end(), val);
	}
}

#endif // AVER_ALGORITHMS_HPP
