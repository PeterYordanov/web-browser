#include "aver_synthesizer.hpp"
#include "core/av_core.hpp"
#include AV_C_API_DEBUG

namespace core::synth
{
	AbstractSynthesizer::AbstractSynthesizer(QObject* parent) : QObject(parent)
	{
	}

#if AV_IS_NIX

	DefaultSynthesizer::DefaultSynthesizer(QObject* parent) : AbstractSynthesizer(parent)
	{
		cst_regex_init();
		m_engine = new DefaultEngine();
		m_engine->voice = nullptr;
		m_engine->wave = nullptr;
		m_engine->volume = AV_TTS_DEFAULT_VOLUME;
		m_engine->sample_rate = AV_TTS_DEFAULT_SAMPLE_RATE;
	}

	void DefaultSynthesizer::setGender(core::synth::AbstractSynthesizer::Gender gender)
	{
		if(__builtin_expect(!!(m_engine->voice != nullptr), AV_FALSE)) {
			if(gender == 0x01)
				unregister_cmu_us_slt(m_engine->voice);
			else if(gender == 0x00)
				unregister_cmu_us_rms(m_engine->voice);
			else
				unregister_cmu_us_kal(m_engine->voice);
		}

		if(gender == core::synth::AbstractSynthesizer::Gender::MALE)
			m_engine->voice = register_cmu_us_slt(nullptr);
		else if(gender == core::synth::AbstractSynthesizer::Gender::FEMALE)
			m_engine->voice = register_cmu_us_rms(nullptr);
		else
			m_engine->voice = register_cmu_us_kal(nullptr);

		m_gender = gender;
	}

	void DefaultSynthesizer::synthesize(const char* text)
	{
		AV_BOOL cond = !(strlen(text) == 0) ? AV_TRUE : AV_FALSE;
		if(__builtin_expect(!!(cond), AV_FALSE)) {
			strcpy(m_engine->local_text, text);
			cst_utterance* utter = flite_synth_text(m_engine->local_text, m_engine->voice);
			if(utter == nullptr)
				return;

			m_engine->wave = utt_wave(utter);

			for (int i = 0; i < m_engine->wave->num_samples; ++i) {
				float volume_factor = m_engine->volume * 65536 / 100;
				m_engine->wave->samples[i] = (int)((m_engine->wave->samples[i] * volume_factor)) / 65536;
			}

			play_wave(m_engine->wave);
			delete_utterance(utter);
		}
	}

	void DefaultSynthesizer::saveToFile(const char* filename)
	{
		if(!(strlen(filename) == 0) && m_engine->wave != nullptr) {
			cst_utterance* utter = flite_synth_text(m_engine->local_text, m_engine->voice);

			m_engine->wave = utt_wave(utter);
			cst_wave_save_riff(m_engine->wave, filename);

			delete_utterance(utter);
		}
	}

	DefaultSynthesizer::~DefaultSynthesizer()
	{
		if(__builtin_expect(!!(m_engine->voice != nullptr), AV_FALSE)) {
			if(m_gender == 0x01)
				unregister_cmu_us_slt(m_engine->voice);
			else if(m_gender == 0x00)
				unregister_cmu_us_rms(m_engine->voice);
			else
				unregister_cmu_us_kal(m_engine->voice);
		}

		delete m_engine;
	}

#else

	DefaultSynthesizer::DefaultSynthesizer(QObject* parent) : AbstractSynthesizer(parent)
	{
		m_engine = new DefaultEngine();
		::CoCreateInstance(CLSID_SpVoice, nullptr,
						   CLSCTX_ALL, IID_ISpVoice,
							(void**)(&m_engine->voice));

		m_engine->volume = AV_TTS_DEFAULT_VOLUME;
		m_engine->sample_rate = AV_TTS_DEFAULT_SAMPLE_RATE;
		m_engine->local_text = new wchar_t[AV_TTS_MAX_BUFFER_SIZE];
	}

	void DefaultSynthesizer::setGender(core::synth::AbstractSynthesizer::Gender gender)
	{
		CComPtr<ISpObjectToken> object_token;
		const WCHAR* gender_literal = (gender == core::synth::AbstractSynthesizer::MALE) ?
					_T("gender = male") : _T("gender = female");

		SpFindBestToken(SPCAT_VOICES, gender_literal, _T(""), &object_token);
		m_engine->voice->SetVoice(object_token);
	}

	void DefaultSynthesizer::synthesize(const char* text)
	{
		if(strlen(text) >= AV_TTS_MAX_BUFFER_SIZE){
			av_debug_set_level(AV_ERROR);
			av_debugf("Input too large! (%d) is the max size", AV_TTS_MAX_BUFFER_SIZE);
			av_debug_set_level(AV_MESSAGE);
			return;
		}

		m_engine->voice->SetVolume((USHORT)m_engine->volume);
		std::size_t out_size;
		std::size_t size = strlen(text) + 1;
		mbstowcs_s(&out_size, m_engine->local_text, size, text, (size - 1) );
		m_engine->voice->Speak(m_engine->local_text, SPF_IS_XML, nullptr);
	}

	void DefaultSynthesizer::saveToFile(const char* filename)
	{
		CSpStreamFormat audio_format;

		char fname[500];
		QString f(filename);
		if(!f.endsWith(".wav")) {
			std::strcpy(fname, filename);
			std::strcat(fname, ".wav");
		}

		audio_format.AssignFormat(SPSF_22kHz16BitMono);
		SPBindToFile(fname, SPFM_CREATE_ALWAYS,
					 &m_engine->stream,
					 &audio_format.FormatId(),
					 audio_format.WaveFormatExPtr());

		m_engine->voice->SetOutput(m_engine->stream, TRUE);
		m_engine->voice->Speak(m_engine->local_text, SpeechVoiceSpeakFlags::SVSFlagsAsync, nullptr);
		m_engine->voice->WaitUntilDone(-1);
	}

	DefaultSynthesizer::~DefaultSynthesizer()
	{
		m_engine->voice->Release();
		m_engine->voice = nullptr;
		delete[] m_engine->local_text;
		delete m_engine;
	}

#endif // AV_IS_NIX
}
