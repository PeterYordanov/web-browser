#ifndef AV_SYSTEM_H
#define AV_SYSTEM_H

/**
 * @file av_system.h
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

#include "av_utils.h"
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#if AV_IS_WINDOWS
#	include <intrin.h>
#	include <Windows.h>
#	include <tchar.h>
#	include <malloc.h>
#else
#	include <cpuid.h>
#endif

#define AV_CPU_BRAND_LENGTH 64
#define AV_CPU_VENDOR_LENGTH AV_CPU_BRAND_LENGTH
#define AV_CPU_REGISTERS 4

/**
 * @brief The registers used to execute CPUID
 */
typedef struct av_registers
{
	uint32_t eax; /** eax */
	uint32_t ebx; /** ebx */
	uint32_t ecx; /** ecx */
	uint32_t edx; /** edx */
} av_registers;

/**
 * @brief A workaround for retrieving the string brand and vendor.
 */
typedef struct av_cpu_str_holder
{
	char brand[AV_CPU_BRAND_LENGTH];
	char vendor[AV_CPU_VENDOR_LENGTH];
} av_cpu_str_holder;

/**
 * @brief The CPU cache.
 */
typedef struct av_cpu_cache
{
	int level; /** Cache level (L1, L2, L3) */
	int associativity; /** Associativity */
	int line_size; /** Cache line size */
	int size; /** Size of the cache itself */
} av_cpu_cache;

/**
 * @brief Holds relevant hardware information.
 */
typedef struct av_system_info
{
	av_cpu_str_holder holder; /** Hold brand and vendor */
	int logical_cores; /** Logical cores */
	int cores; /** Physical cores */
	int page_size; /** Page Size */
	unsigned long long ram_kilobytes; /** RAM in kb */
	av_cpu_cache l1; /** L1 Cache */
	av_cpu_cache l2; /** L2 Cache */
	av_cpu_cache l3; /** L3 Cache */
} av_system_info;


#ifdef __cplusplus
extern "C"
{
#endif
/**
 * @brief Initializes the av_system_info structure
 * @param _this The address of the structure
 */
void				av_system_info_init(av_system_info* _this);

/**
 * @brief RAM in kilobytes
 * @param _this The address of the structure
 */
unsigned long long  av_system_get_ram_kb(av_system_info* _this);

/**
 * @brief RAM in gigabytes
 * @param _this The address of the structure
 */
int					av_system_get_ram_gb(av_system_info* _this);

/**
 * @brief Page size
 * @param _this The address of the structure
 */
int					av_system_get_page_size(av_system_info* _this);


unsigned long	    av_system_get_oemid(void);

/**
 * @brief CPU Brand name
 * @param _this The address of the structure
 */
av_cpu_str_holder	av_system_get_cpu_brand_name(av_system_info* _this);

/**
 * @brief CPU Vendor
 * @param _this The address of the structure
 */
av_cpu_str_holder	av_system_get_vendor(av_system_info* _this);

/**
 * @brief CPU Logical cores
 * @param _this The address of the structure
 */
int					av_system_get_logical_cores(av_system_info* _this);

/**
 * @brief CPU Physical cores
 * @param _this The address of the structure
 */
int					av_system_get_cores(av_system_info* _this);

/**
 * @brief CPU cache info for a specific cache level
 * @param _this The address of the structure
 * @param level Cache level
*/
av_cpu_cache		av_system_get_cache_info(av_system_info* _this, int level);

/**
 * @brief Deinitializes the av_system_info structure
 * @param _this The address of the structure
 */
void				av_system_info_uninit(av_system_info* _this);
#ifdef __cplusplus
}
#endif

/**
 * @brief Holds the CPU registers to write to and read from
 */
typedef struct av_cpu_features
{
	av_registers regs;
} av_cpu_features;


#ifdef __cplusplus
extern "C"
{
#endif
/**
 * @brief Executes the CPUID instruction
 * @param regs The eax, ebx, ecx, edx registers
 * @param level CPUID level
 */
void av_cpuid(av_registers* regs, unsigned int level);

/**
 * @brief Initializes the struct
 * @param _this The address of the structure
 */
void		av_cpu_features_init(av_cpu_features* _this);

/**
 * @brief Checks if SSE is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_sse_supported(av_cpu_features* _this);

/**
 * @brief Checks if SSE2 is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_sse2_supported(av_cpu_features* _this);

/**
 * @brief Checks if SSE3 is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_sse3_supported(av_cpu_features* _this);

/**
 * @brief Checks if SSE4.1 is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_sse41_supported(av_cpu_features* _this);

/**
 * @brief Checks if SSE4.2 is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_sse42_supported(av_cpu_features* _this);

/**
 * @brief Checks if AVX is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_avx_supported(av_cpu_features* _this);

/**
 * @brief Checks if AVX2 is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_avx2_supported(av_cpu_features* _this);

/**
 * @brief Checks if MMX is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_mmx_supported(av_cpu_features* _this);

/**
 * @brief Checks if Hyper Threading is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_hyper_threading_supported(av_cpu_features* _this);

/**
 * @brief Checks if FPU is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_fpu_supported(av_cpu_features* _this);

/**
 * @brief Checks if PCLMULQDQ is supported
 * @param _this The address of the structure
 */
const char* av_cpu_is_pclmulqdq_supported(av_cpu_features* _this);

#ifdef __cplusplus
}
#endif

#endif // AV_SYSTEM_H
