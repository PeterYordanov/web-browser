#ifndef AV_DEBUG_H
#define AV_DEBUG_H

/**
 * @file av_debug.h
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

/**
 * @brief Normal console color
 */
#define AV_DEBUG_COLOR_NORMAL "\x1B[0m"

/**
 * @brief Red console color
 */
#define AV_DEBUG_COLOR_RED "\x1B[31m"

/**
 * @brief Green console color
 */
#define AV_DEBUG_COLOR_GREEN "\x1B[32m"

/**
 * @brief Yellow console color
 */
#define AV_DEBUG_COLOR_YELLOW "\x1B[33m"

/**
 * @brief Blue console color
 */
#define AV_DEBUG_COLOR_BLUE "\x1B[34m"

/**
 * @brief Magenta console color
 */
#define AV_DEBUG_COLOR_MAGENTA "\x1B[35m"

/**
 * @brief Cyan console color
 */
#define AV_DEBUG_COLOR_CYAN "\x1B[36m"

/**
 * @brief White console color
 */
#define AV_DEBUG_COLOR_WHITE "\x1B[37m"

/**
 * @brief Debug level
 */
typedef enum Level {
	AV_MESSAGE = 1 << 1,
	AV_WARNING = 1 << 2,
	AV_ERROR   = 1 << 3,
	AV_DEFAULT = 1 << 4
} Level;

static int level = AV_DEFAULT;

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Prints to standard output
 * @param format Format.
 * @param ...
 */
void av_printf(const char* format, ...);

/**
 * @brief Prints to standard output
 * @param format Format.
 * @param ...
 */
void av_debugf(const char* format, ...);

/**
 * @brief Sets the debug level.
 * @param lvl Debug level
 */
void av_debug_set_level(Level lvl);

/**
 * @brief Retrieves debug level.
 */
/*inline int av_debug_level(void)
{
	return level;
}*/
#ifdef __cplusplus
}
#endif

#endif // AV_DEBUG_H
