#ifndef AV_SIGNAL_HANDLER_H
#define AV_SIGNAL_HANDLER_H

/**
 * @file av_signal_handler.h
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

/**
 * @brief Size of the stack to capture
 */
#define AV_MAX_STACK_SIZE 1024

/**
 * @brief Maximum length for symbol/func name
 */
#define AV_MAX_SYMBOL_NAME_LENGTH (AV_MAX_STACK_SIZE >> 2)

#define AV_SIGNAL_PROGRAM_INTERRUPT 2
#define AV_SIGNAL_ILLEGAL_INSTRUCTION 4
#define AV_SIGNAL_ABORT 22
#define AV_SIGNAL_FP_EXCEPTION 8
#define AV_SIGNAL_SEG_FAULT 11
#define AV_SIGNAL_TERMINATION 15
#define AV_SIGNAL_STACK_FAULT 16

#include <signal.h>
#include "av_debug.h"
#include <stdlib.h>
#include "av_utils.h"

/**
 * @brief Where to save debug.txt to
 */
#define AV_SIGNAL_DEBUG_FILE_PATH AV_STR_EXPAND(debug.txt)
#define AV_SIGNAL_DEBUG_FILE_WRITE AV_STR_EXPAND(w)

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Registers all handled signals.
 */
void			 av_register_signals(void);
AV_NORETURN void av_signal_callback(int signum);

/**
 * @brief Prints the entire stack trace
 */
void			 av_print_stack_trace(void);
const char*		 _av_fetch_signal_name(int signum);
#ifdef __cplusplus
}
#endif

#endif // AV_SIGNAL_HANDLER_H
