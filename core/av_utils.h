#ifndef AV_UTILS_H
#define AV_UTILS_H

/**
 * @file av_utils.h
 * @author Peter Yordanov
 * @date 05 January 2019
 * @brief 
 *
 * @see .html
 * @see .html
 */

#include <QtGlobal>

//On *nix, the least significant nibble will be set
#if defined(Q_OS_LINUX) || defined(Q_OS_UNIX) || defined(__GNUC__)
#	define CURRENT_PLATFORM (0x00 | 0xF) //0xF
#	define CURRENT_COMPILER (0x00 | 0xF)
#endif
//On Windows, the most significant nibble will be set
#if defined(Q_OS_WIN32) || defined(Q_OS_WIN64) || defined(_MSC_VER)
#	define CURRENT_PLATFORM ((0x00 | 0xF) << 4) //0xF0
#	define CURRENT_COMPILER ((0x00 | 0xF) << 4)
#endif

/**
 @brief Removes the "unused variable" compiler warning
 */
#define AV_UNUSED(x) (void)x


/**
 * @brief Frees allocated memory and sets it to null to avoid dangling pointers
 * @param ptr The pointer to free
 */
#define AV_SAFE_FREE(ptr) \
	if(ptr) { \
		free(ptr); \
		ptr = NULL; \
	}

/**
 * @brief Bool, the C standard doesn't specify the size of its bool... Which is in a header file.
 */
typedef enum _bool {
	AV_FALSE = 0x0,
	AV_TRUE
} AV_BOOL;

/**
 * @brief Extracts the high nibble of a byte.
 * @param b A byte to inspect.
 */
#define HIGH_NIBBLE(b) ((b) & (0x0F << 4))

/**
 * @brief Extracts the low nibble of a byte.
 * @param b A byte to inspect.
 */
#define LOW_NIBBLE(b) ((b) & 0x0F)

/**
 * @brief Stringifies a token.
 * @param token Token.
 */
#define AV_STR_EXPAND(token) #token

/**
 * @brief Returns if the current platform is Windows.
 */
#define AV_IS_WINDOWS HIGH_NIBBLE(CURRENT_PLATFORM)

/**
 * @brief Returns if the current platform is Unix-like.
 */
#define AV_IS_NIX LOW_NIBBLE(CURRENT_PLATFORM)

/**
 * @brief Returns if the current compiler is MSVC.
 */
#define AV_IS_MSVC HIGH_NIBBLE(CURRENT_COMPILER)

/**
 * @brief Returns if the current compiler GCC.
 */
#define AV_IS_GCC LOW_NIBBLE(CURRENT_COMPILER)

#define AV_TTS_MAX_BUFFER_SIZE 0x1 << 13
#define AV_TTS_DEFAULT_VOLUME 50
#define AV_TTS_DEFAULT_SAMPLE_RATE 0x00
#define AV_EMPTY_STRING ""

#if AV_IS_GCC
#define AV_NORETURN __attribute__((__noreturn__))
#elif AV_IS_MSVC
#define AV_NORETURN __declspec(noreturn)
#endif

#define Q_TEST_OBJECT Q_OBJECT

#define AV_RESOURCES_PATH_RELATIVE "../aver/deps/resources/"

#define AV_FOREVER for(;;)

/**
 * @brief For test method declaration
 * @code AV_TEST_SLOTS:
 * @code	void testCase();
 * @code	...
 */
#define AV_TEST_SLOTS private Q_SLOTS

#endif // AV_UTILS_H
