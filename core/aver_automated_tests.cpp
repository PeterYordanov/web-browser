#include "aver_automated_tests.hpp"
#include <QtTest/QtTest>
#include "core/av_core.hpp"
#include AV_C_API_DEBUG
#include <QObject>

namespace core::autotests
{
	int QAutomatedTestsManager::run(int argc, char* argv[])
	{
		int ret = 0;

		for(auto& test : m_testsList) {
			ret += QTest::qExec(test, argc, argv);
			av_debugf("\n\n");
		}

		return ret;
	}

	void QAutomatedTestsManager::addTest(QObject* test)
	{
		if(test)
			m_testsList.append(test);
	}

	void QAutomatedTestsManager::removeTest(QObject* test)
	{
		if(test) {
			for(int i = 0; i <= m_testsList.count(); i++)
				if(m_testsList.at(i) == test) {
					m_testsList.removeAt(i);
				}
		}
	}

}
