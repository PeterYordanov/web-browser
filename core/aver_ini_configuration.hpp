#ifndef AVER_INI_PARSER_HPP
#define AVER_INI_PARSER_HPP

/**
 * @file aver_ini_configuration.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief Reads and Writes INI config files
 *
 * @see .html
 * @see .html
 */

#include "aver_textfile.hpp"
#include <QList>
#include <QDebug>
#include <QMap>
#include <QChar>
#include <QVariant>

namespace core::ini
{
	/**
	 * @brief Pair of a key and value
	 */
	struct KeyValuePair
	{
		QString key;
		QVariant value;

		explicit KeyValuePair(QString line);
	};

	/**
	 * @brief Contains sections info
	 */
	struct Section
	{
		QString sectionName;
		QList<KeyValuePair> sectionContents;

		void parse(const QStringList& text);
	};

	/**
	 * @brief INI file reader
	 */
	class INIParser
	{
	public:
		explicit INIParser(const char* filename);
		Section section(const QString& sectionName)
		{
			for(auto& i : m_sections) {
				if(i.sectionName == sectionName)
					return i;
			}
		}
		void pair(int index){ (void)index; }

	private:
		QList<Section> m_sections;
		core::TextFileReader m_reader;
	};

	/**
	 * @brief Writes to INI file
	 */
	class INIWriter
	{
	public:
		explicit INIWriter(const char* config);

		/**
		 * @brief Starts the ini file
		 */
		void startINI();

		/**
		 * @brief Writes out the section name
		 * @param sectionName The section name
		 */
		void startSection(const QString& sectionName);

		/**
		 * @brief Writes out the pair of key and value
		 * @param key INI Key
		 * @param value INI Value
		 */
		void addPair(const QString& key, const QVariant& value);

		/**
		 * @brief Ends a section
		 */
		void endSection();

		/**
		 * @brief Generates the INI file
		 */
		void endINI();

	private:
		QStringList m_ini;
		core::TextFileWriter m_writer;
	};
}

#endif // AVER_INI_PARSER_HPP
