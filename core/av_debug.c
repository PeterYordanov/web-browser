#include "av_debug.h"
#include <stdarg.h>
#include <stdio.h>

void av_printf(const char* format, ...)
{
	va_list arglist;
	va_start(arglist, format);
	vprintf(format, arglist);
	va_end(arglist);
}

void av_debugf(const char* format, ...)
{
	switch(level)
	{
		case AV_ERROR:
			av_printf("%s", AV_DEBUG_COLOR_RED);
			av_printf("ERROR : ");
			break;
		case AV_WARNING:
			av_printf("%s", AV_DEBUG_COLOR_YELLOW);
			av_printf("WARNING : ");
			break;
		case AV_MESSAGE:
			av_printf("%s", AV_DEBUG_COLOR_BLUE);
			av_printf("MESSAGE : ");
			break;
		case AV_DEFAULT:
			av_printf("%s", AV_DEBUG_COLOR_NORMAL);
			break;
	}

	va_list arglist;
	va_start(arglist, format);
	vprintf(format, arglist);
	va_end(arglist);
	level = AV_DEFAULT;
}

void av_debug_set_level(Level lvl)
{
	if(!(lvl < (1 << 1)) &&
	   !(lvl > (1 << 4)))
		level = lvl;
}
