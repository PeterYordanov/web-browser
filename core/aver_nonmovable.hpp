#ifndef AVER_NONMOVABLE_HPP
#define AVER_NONMOVABLE_HPP

/**
 * @file aver_noncopy_nonmove.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

namespace core {
	
	/**
	 * @brief Any class that inherits from it will not be able to be std::move-ed
	 */
	class NonMovable
	{
	#if __cplusplus >= 201103L
	public:
		NonMovable() = default;
		NonMovable(NonMovable&&) = delete;
		NonMovable& operator=(NonMovable&&) = delete;
	#endif //__cplusplus >= 201103L
	//Pre-C++11 does not support rvalue references, hence move semantics as well
	};
}

#endif // AVER_NONMOVABLE_HPP
