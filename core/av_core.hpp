#ifndef AV_CORE_HPP
#define AV_CORE_HPP

//This file is absolutely beautiful, lol

#include "core/av_utils.h"

#define AV_HAVE_ALGORITHMS
#define AV_HAVE_DEBUG
#if AV_IS_WINDOWS
#define AV_HAVE_SAPI
#endif // AV_IS_WINDOWS
#if AV_IS_NIX
#define AV_HAVE_FLITE
#endif // AV_IS_NIX
#define AV_HAVE_AUTOMATED_TESTING
#define AV_HAVE_NONCOPYABLE
#define AV_HAVE_NONMOVABLE
#define AV_HAVE_SYSTEM
#define AV_HAVE_UTILITIES
#define AV_HAVE_SIGNAL_HANDLER
//#define AV_HAVE_ARCHIVER
#define AV_HAVE_INI
#define AV_HAVE_TEXT_FILE

//C
#ifdef AV_HAVE_DEBUG
#	define AV_C_API_DEBUG AV_STR_EXPAND(core/av_debug.h)
#endif //AV_HAVE_DEBUG
/*#ifdef AV_HAVE_FLITE
#	define AV_C_API_FLITE AV_STR_EXPAND(core/av_flite.h)
#endif // AV_HAVE_FLITE
#ifdef AV_HAVE_SAPI
#	define AV_C_API_SAPI AV_STR_EXPAND(core/av_sapi.h)
#endif // AV_HAVE_SAPI
*/
#ifdef AV_HAVE_SIGNAL_HANDLER
#	define AV_C_API_SIGNAL_HANDLER AV_STR_EXPAND(core/av_signal_handler.h)
#endif // AV_HAVE_SIGNAL_HANDLER
#ifdef AV_HAVE_SYSTEM
#	define AV_C_API_SYSTEM AV_STR_EXPAND(core/av_system.h)
#endif // AV_HAVE_SYSTEM
#ifdef AV_HAVE_UTILITIES
#	define AV_C_API_UTILITIES AV_STR_EXPAND(core/av_utils.h)
#endif // AV_HAVE_UTILITIES

//C++
#if defined(AV_HAVE_NONMOVABLE) && defined(AV_HAVE_NONCOPYABLE)
#	define AV_CPP_API_NONCOPYABLE AV_STR_EXPAND(core/aver_noncopyable.hpp)
#	define AV_CPP_API_NONMOVABLE AV_STR_EXPAND(core/aver_nonmovable.hpp)
#endif // defined(AV_HAVE_NONMOVABLE) || defined(AV_HAVE_NONCOPYABLE)
#if defined(AV_HAVE_SAPI) || defined(AV_HAVE_FLITE)
#	define AV_CPP_API_SYNTHESIZER AV_STR_EXPAND(core/aver_synthesizer.hpp)
#endif // defined(AV_HAVE_SAPI) || defined(AV_HAVE_FLITE)
#ifdef AV_HAVE_AUTOMATED_TESTING
#	define AV_CPP_API_AUTOMATED_TESTING AV_STR_EXPAND(core/aver_automated_tests.hpp)
#endif // AV_HAVE_AUTOMATED_TESTING
#ifdef AV_HAVE_SYSTEM
#	define AV_CPP_API_SYSTEM AV_STR_EXPAND(core/aver_system_info_proxy.hpp)
#endif // AV_HAVE_SYSTEM
#ifdef AV_HAVE_ALGORITHMS
#	define AV_CPP_API_ALGORITHMS AV_STR_EXPAND(core/aver_algorithms.hpp)
#endif // AV_HAVE_ALGORITHMS
#ifdef AV_HAVE_INI
#	define AV_CPP_API_INI AV_STR_EXPAND(core/aver_ini_configuration.hpp)
#endif // AV_HAVE_INI
#ifdef AV_HAVE_ARCHIVER
#	define AV_CPP_API_ARCHIVER AV_STR_EXPAND(core/aver_archive.hpp)
#endif //AV_HAVE_ARCHIVER
#ifdef AV_HAVE_TEXT_FILE
#	define AV_CPP_API_TEXT_FILE AV_STR_EXPAND(core/aver_textfile.hpp)
#endif // AV_HAVE_TEXT_FILE

#include <QDir>

inline QString toAbsolutePath(const QString& p)
{
	QDir dir;
	return dir.absoluteFilePath(p);
}

#define AV_TO_ABSOLUTE_PATH(x) toAbsolutePath(x)
#define AV_GET_RESOURCE_PATH(x) AV_TO_ABSOLUTE_PATH(QString(QString(AV_RESOURCES_PATH_RELATIVE) + QString(x)))

#endif // AV_CORE_HPP
