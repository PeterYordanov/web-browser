#ifndef AVER_THREAD_HPP
#define AVER_THREAD_HPP

/**
 * @file aver_thread.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

#include <pthread.h>

#endif // AVER_THREAD_HPP
