#include "aver_archive.hpp"
//#include "libarchive.hpp"
#include <QByteArray>
#include <QDebug>
#include <QFile>
#include "core/av_core.hpp"
#include AV_C_API_UTILITIES

namespace core::archive
{
	Archive::Archive(const QString& archive, QObject* parent) :
		QObject(parent)
	{
		if(archive != m_archivePath)
			m_archivePath = archive;

		QByteArray bArray = QFile::encodeName(m_archivePath);
		const char* archivePath = bArray.data();

		m_archive = archive_read_new();
		archive_read_support_format_all(m_archive);
		archive_read_support_filter_all(m_archive);
		archive_read_open_filename(m_archive, archivePath, 16384);

		m_disk = archive_write_disk_new();

		archive_write_disk_set_options(m_disk, ARCHIVE_EXTRACT_TIME |
											   ARCHIVE_EXTRACT_PERM |
											   ARCHIVE_EXTRACT_ACL  |
											   ARCHIVE_EXTRACT_FFLAGS);

		archive_write_disk_set_standard_lookup(m_disk);

		m_isOpened = true;
		m_type = TypeOfArchiveData::FileData;
	}

	Archive::~Archive()
	{
		archive_read_close(m_archive);
		archive_read_free(m_archive);

		archive_write_close(m_disk);
		archive_write_free(m_disk);

		m_isOpened = false;
	}

	void Archive::extractTo(const QString& path)
	{
		struct archive_entry* entry;
		m_extractToPath = path;

		auto file_exists = [](const QString& path) -> bool {
			QFile file(path, Q_NULLPTR);
			return file.exists();
		};

		auto archive_copy_data = [](struct archive* from, struct archive* to) {
			int			status;
			const void* buff;
			size_t		size;
			la_int64_t	offset;

			AV_FOREVER {
				status = archive_read_data_block(from, &buff, &size, &offset);
				if (status == ARCHIVE_EOF)
					return (ARCHIVE_OK);

				if (status < ARCHIVE_OK) {
					qDebug() << archive_error_string(from);
					return (status);
				}

				status = (la_ssize_t)archive_write_data_block(to, buff, size, offset);
				if (status < ARCHIVE_OK) {
					qDebug() << archive_error_string(to);
					return (status);
				}
			}
		};

		while(archive_read_next_header(m_archive, &entry) == ARCHIVE_OK) {

			QString diskExtractPath = path;
			if(!diskExtractPath.endsWith("/")) {
				diskExtractPath.append("/");
			}

			diskExtractPath.append(archive_entry_pathname(entry));

			QByteArray bArray = QFile::encodeName(diskExtractPath);
			const char* filePath = bArray.data();
			//no reason to extract if it's already there
			if(file_exists(filePath)) continue;

			archive_entry_set_pathname(entry, filePath);

			archive_write_header(m_disk, entry);
			if(m_type == FileData)
				archive_copy_data(m_archive, m_disk);

			archive_write_finish_entry(m_disk);

			//Q_EMIT fileExtracted(diskExtractPath);
		}
	}
}

