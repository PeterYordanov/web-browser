#ifndef AVER_SYSTEM_INFO_PROXY_HPP
#define AVER_SYSTEM_INFO_PROXY_HPP


/**
 * @file aver_system_info_proxy.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief Wrapper around the C API av_system
 *
 * @see .html
 * @see .html
 */

#include "core/av_system.h"
#include <QString>

//Papa bless inline namespaces
namespace core::system
{
	/**
	 * @brief Returns which CPU features are supported
	 */
	class CPUFeaturesProxy
	{
	public:
		inline CPUFeaturesProxy()
		{
			av_cpu_features_init(&m_cpu);
		}

		~CPUFeaturesProxy() = default;

		/**
		 * @brief Returns if SSE is supported
		 * @return ''
		 */
		inline QString isSSESupported()
		{ return QString(av_cpu_is_sse_supported(&m_cpu)); }

		/**
		 * @brief Returns if SSE2 is supported
		 * @return ''
		 */
		inline QString isSSE2Supported()
		{ return QString(av_cpu_is_sse2_supported(&m_cpu)); }

		/**
		 * @brief Returns if SSE3 is supported
		 * @return ''
		 */
		inline QString isSSE3Supported()
		{ return QString(av_cpu_is_sse3_supported(&m_cpu)); }

		/**
		 * @brief Returns if SSE4.1 is supported
		 * @return ''
		 */
		inline QString isSSE41Supported()
		{ return QString(av_cpu_is_sse41_supported(&m_cpu)); }

		/**
		 * @brief Returns if SSE4.2 is supported
		 * @return ''
		 */
		inline QString isSSE42Supported()
		{ return QString(av_cpu_is_sse42_supported(&m_cpu)); }

		/**
		 * @brief Returns if MMX is supported
		 * @return ''
		 */
		inline QString isMMXSupported()
		{ return QString(av_cpu_is_mmx_supported(&m_cpu)); }

		/**
		 * @brief Returns if AVX is supported
		 * @return ''
		 */
		inline QString isAVXSupported()
		{ return QString(av_cpu_is_avx_supported(&m_cpu)); }

		/**
		 * @brief Returns if AVX2 is supported
		 * @return ''
		 */
		inline QString isAVX2Supported()
		{ return QString(av_cpu_is_avx2_supported(&m_cpu)); }

		/**
		 * @brief Returns if Hyper Threading is supported
		 * @return ''
		 */
		inline QString isHyperThreadingSupported()
		{ return av_cpu_is_hyper_threading_supported(&m_cpu); }

		/**
		 * @brief Returns if PCLMULQDQ is supported
		 * @return ''
		 */
		inline QString isPCLMULQDQSupported()
		{ return av_cpu_is_pclmulqdq_supported(&m_cpu); }

		/**
		 * @brief Returns if FPU is supported
		 * @return ''
		 */
		inline QString isFPUSupported()
		{ return av_cpu_is_fpu_supported(&m_cpu); }

	private:
		av_cpu_features m_cpu;
	};

	/**
	 * @brief Returns hardware specifications
	 */
	class SystemInfoProxy
	{
	public:
		inline SystemInfoProxy()
		{
			av_system_info_init(&m_system);
		}

		inline ~SystemInfoProxy()
		{
			av_system_info_uninit(&m_system);
		}

		/**
		 * @brief Returns RAM in GBs
		 * @return ''
		 */
		inline int RAMGigaBytes()
		{ return av_system_get_ram_gb(&m_system); }

		/**
		 * @brief Returns RAM in KBs
		 * @return ''
		 */
		inline unsigned long long RAMKiloBytes()
		{ return av_system_get_ram_kb(&m_system); }

		/**
		 * @brief Returns the page size
		 * @return ''
		 */
		inline int pageSize()
		{ return (int)av_system_get_page_size(&m_system); }

		/**
		 * @brief Returns the CPU brand name
		 * @return ''
		 */
		inline QString brandName()
		{ return av_system_get_cpu_brand_name(&m_system).brand; }

		/**
		 * @brief Returns logical cores
		 * @return ''
		 */
		inline int logicalCores()
		{ return av_system_get_logical_cores(&m_system); }

		/**
		 * @brief Returns physical cores
		 * @return ''
		 */
		inline int cores()
		{ return av_system_get_cores(&m_system); }

		/**
		 * @brief Returns cache information
		 * @param level Cache level
		 */
		inline av_cpu_cache cache(int level)
		{ return av_system_get_cache_info(&m_system, level); }

		/**
		 * @brief Returns CPU vendor name
		 * @return ''
		 */
		inline QString vendorName()
		{ return av_system_get_vendor(&m_system).vendor;}

	private:
		av_system_info m_system;
	};

	typedef SystemInfoProxy SystemInfo;
	typedef CPUFeaturesProxy CPUFeatures;
}

#endif // AVER_SYSTEM_INFO_PROXY_HPP
