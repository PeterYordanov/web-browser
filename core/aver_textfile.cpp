#include "aver_textfile.hpp"
#include <QFile>
#include <QTextStream>

namespace core {

	TextFileReader::TextFileReader(QObject* parent) :
		QObject(parent),
		m_lines(0),
		m_content(),
		m_filePath()
	{}

	void TextFileReader::setPath(const QString& path)
	{
		if(!path.isEmpty())
			m_filePath = path;
	}

	QStringList TextFileReader::read()
	{
		if(!m_content.isEmpty()) {
			m_content.clear();
		}

		m_lines = 0;
		QFile file(m_filePath);

		if(file.exists()) {
			file.open(QIODevice::ReadOnly | QIODevice::Text);
			QTextStream stream(&file);

			while(!stream.atEnd()) {
				QString text = stream.readLine();
				m_lines++;
				m_content << text;
			}

			return m_content;
		}

		file.close();

		return QStringList();
	}

	QStringList TextFileReader::readUntilLine(const unsigned int index)
	{
		(void)index;
		return QStringList();
	}

	QString TextFileReader::readAtLine(const unsigned int index)
	{
		(void)index;
		return QString();
	}

	TextFileWriter::TextFileWriter(QObject* parent) :
		QObject(parent),
		m_content(),
		m_lines(0)
	{

	}

	void TextFileWriter::setText(const QStringList& text)
	{
		if(!text.isEmpty())
			m_content = text;
	}

	void TextFileWriter::setPath(const QString& path)
	{
		if(!path.isEmpty())
			m_filePath = path;
	}

	void TextFileWriter::write()
	{
		m_lines = 0;
		QFile file(m_filePath);

		if(file.open(QIODevice::WriteOnly)) {
			QTextStream stream(&file);

			for(QString& str : m_content) {
				stream << str;
				stream << NEW_LINE;
				m_lines++;
			}

			file.close();
		}
	}

	void TextFileWriter::writeAtLine(const unsigned int line)
	{
		QFile file(m_filePath);
		if(file.open(QIODevice::WriteOnly)) {
			QTextStream stream(&file);
			stream << m_content.at(line - 1);
			file.close();
		}
	}

	void TextFileWriter::writeUntilLine(const unsigned int line)
	{
		QFile file(m_filePath);
		if(file.open(QIODevice::WriteOnly)) {
			QTextStream stream(&file);
			for(int i = 0; i != line; i++) {
				stream << m_content.at(i);
				stream << NEW_LINE;
			}

			file.close();
		}
	}
}
