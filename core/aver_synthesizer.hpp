#ifndef AVER_SYNTHESIZER_HPP
#define AVER_SYNTHESIZER_HPP

/**
 * @file aver_synthesizer.hpp
 * @author Peter Yordanov
 * @date 13 February 2019
 * @brief
 *
 * @see .html
 * @see .html
 */

#include <QObject>
#include "core/av_core.hpp"
#include AV_C_API_UTILITIES

#if AV_IS_WINDOWS
#pragma warning (disable:4996)
#include <sapi.h> //ISpVoice
#include <atlbase.h> //CComModule
#include <sphelper.h> //CSpStreamFormat
#include <string.h>
#include <memory.h>
#include <stdio.h>
#else
extern "C"
{
	#include <flite.h>
	#include <flite_version.h>
	#include <cst_tokenstream.h>
	#include <cst_alloc.h>
	#include <cst_clunits.h>
	#include <cst_cg.h>

	cst_voice* register_cmu_us_rms(const char* voxdir);
	void unregister_cmu_us_rms(cst_voice* v);
	cst_voice* register_cmu_us_slt(const char* voxdir);
	void unregister_cmu_us_slt(cst_voice* v);
	cst_voice* register_cmu_us_kal(const char* voxdir);
	void unregister_cmu_us_kal(cst_voice* v);
	cst_voice* register_cmu_us_awb(const char* voxdir);
	void unregister_cmu_us_awb(cst_voice* v);
}
#endif

namespace core::synth
{
	/**
	 * @brief All synthesizer implementations inherit from that class, supports Qt's memory management.
	 * @param name Class name of the test
	 */
	class AbstractSynthesizer : public QObject
	{
		Q_OBJECT
	public:
		/**
		 * @brief Gender to set
		 */
		typedef enum Gender
		{
			MALE, /** Male voice */
			FEMALE, /** Female voice */
			UNKNOWN
		} Gender;

		/**
		 * @param parent QObject parent
		 */
		explicit AbstractSynthesizer(QObject* parent = nullptr);

		/**
		 * @brief Sets the volume for the TTS engine
		 * @param volume The volume
		 */
		virtual void setVolume(int volume) = 0;

		/**
		 * @brief Sets the sample rate for the TTS engine
		 * @param sample_rate The sample rate
		 */
		virtual void setSampleRate(int sample_rate) = 0;

		/**
		 * @brief Sets the gender for the TTS engine
		 * @param gender The gender (0 for male, 1 for female)
		 */
		virtual void setGender(core::synth::AbstractSynthesizer::Gender gender) = 0;

		/**
		 * @brief Synthesizes the passed text
		 * @param text Text to say out loud
		 */
		virtual void synthesize(const char* text) = 0;

		/**
		 * @brief Saves the last said sentence/word to an audio file (Saves as .wav on Windows and .tiff on Linux)
		 * @param filename Path and filename (Ending with format is not needed)
		 */
		virtual void saveToFile(const char* filename) = 0;

		/**
		 * @brief Returns the current engine
		 * @return ''
		 */
		virtual QString engine() const = 0;

		~AbstractSynthesizer() = default;
	};

	class DefaultSynthesizer : public AbstractSynthesizer
	{
	public:
		explicit DefaultSynthesizer(QObject* parent = nullptr);

		inline void setVolume(int volume) Q_DECL_OVERRIDE
		{
			m_engine->volume = volume;
		}

		inline void setSampleRate(int sample_rate) Q_DECL_OVERRIDE
		{
			m_engine->sample_rate = sample_rate;
		}

		void setGender(core::synth::AbstractSynthesizer::Gender gender) Q_DECL_OVERRIDE;
		void synthesize(const char*) Q_DECL_OVERRIDE;
		void saveToFile(const char* filename) Q_DECL_OVERRIDE;

		QString engine() const Q_DECL_OVERRIDE
		{ return AV_IS_WINDOWS ? "SAPI" : "flite"; }

		virtual ~DefaultSynthesizer() Q_DECL_OVERRIDE;

	private:
		typedef struct Def
		{
#if AV_IS_NIX
			cst_voice* voice;
			cst_wave* wave;
			char local_text[AV_TTS_MAX_BUFFER_SIZE];
#else
			ISpVoice* voice;
			ISpStream* stream;
			ISpObjectToken* object_token;
			wchar_t* local_text;
#endif
			int volume;
			int rate;
			int sample_rate;
		} DefaultEngine;

		DefaultEngine* m_engine;
		core::synth::AbstractSynthesizer::Gender m_gender;
	};

	class AWSSynthesizer : public AbstractSynthesizer
	{
	public:
		inline void setVolume(int volume) Q_DECL_OVERRIDE
		{
			(void)volume;
		}

		inline void setSampleRate(int sample_rate) Q_DECL_OVERRIDE
		{
			(void)sample_rate;
		}

		void setGender(core::synth::AbstractSynthesizer::Gender gender) Q_DECL_OVERRIDE;
		void synthesize(const char*) Q_DECL_OVERRIDE;
		void saveToFile(const char* filename) Q_DECL_OVERRIDE;

		QString engine() const Q_DECL_OVERRIDE
		{ return AV_IS_WINDOWS ? "SAPI" : "flite"; }

		virtual ~AWSSynthesizer() Q_DECL_OVERRIDE;
	};

	/**
	 * @brief Factory method
	 */
	class SynthesizerFactory
	{
	public:
		/**
		 * @brief Synthesizers supported
		 */
		typedef enum Synthesizers
		{
			Native, /** Flite on Linux, SAPI on Windows */
			AWS /** AWS SDK engine */
		} Synthesizers;

		SynthesizerFactory() = default;
		~SynthesizerFactory() = default;

		/**
		 * @brief Factory Method to create a synthesizer
		 * @param s The type of synthesizer to create
		 * @param parent QObject parent, optional, used for Qt's memory management
		 */
		inline static AbstractSynthesizer* createSynthesizer(core::synth::SynthesizerFactory::Synthesizers s, QObject* parent = nullptr)
		{
			switch (s)
			{
				case Native:
					return new DefaultSynthesizer(parent);
				case AWS:
					return nullptr;
			}

			return nullptr;
		}
	};

}

#endif // AVER_SYNTHESIZER_HPP
