#include "aver_dialogs.hpp"
#include <QIcon>
#include <QPushButton>
#include <QHBoxLayout>
#include <QLabel>
#include <QUrl>
#include <QDir>
#include <QWebEngineView>
#include <QImage>
#include "core/av_core.hpp"
#include AV_CPP_API_SYSTEM
#include <QMenu>
#include <QMenuBar>

#include <QFileDialog>
#include <QString>
#include <QPlainTextEdit>
#include "core/av_core.hpp"
#include AV_CPP_API_TEXT_FILE
#include <QVBoxLayout>
#include <QTabWidget>
#include <QToolBar>
#include <QMessageBox>
//#include AV_CPP_API_ARCHIVER
#include <QFileSystemModel>
#include <QTreeView>

TextEditorWidget::TextEditorWidget(QWidget* parent) :
	QTabWidget(parent)
{
	m_vboxLayout = new QVBoxLayout(this);

	setDocumentMode(true);
	setElideMode(Qt::ElideRight);

	QTabBar* tabbar = this->tabBar();
	tabbar->setTabsClosable(true);
	tabbar->setSelectionBehaviorOnRemove(QTabBar::SelectPreviousTab);
	tabbar->setMovable(true);
	tabbar->setContextMenuPolicy(Qt::CustomContextMenu);

	QObject::connect(tabbar, &QTabBar::tabCloseRequested, this, &TextEditorWidget::closeFile);

}

void TextEditorWidget::openFile(const QString& path)
{
	QPlainTextEdit* textEditWidget = new QPlainTextEdit(this);
	textEditWidget->setToolTip(path);

	QString tabName = path.split("/").last();
	addTab(textEditWidget, tabName);

	core::TextFileReader reader;
	reader.setPath(path);
	QStringList text = reader.read();
	m_textList.append(text);

	for(const auto& line : text) {
		textEditWidget->appendPlainText(line);
	}
}

void TextEditorWidget::saveFile(int index)
{
	QPlainTextEdit* textEditWidget = dynamic_cast<QPlainTextEdit*>(this->widget(index));

	QString path = textEditWidget->toolTip();
	QStringList text = textEditWidget->document()
						->toPlainText().split("\n");

	core::TextFileWriter writer;
	writer.setPath(path);
	writer.setText(text);
	writer.write();
}

void TextEditorWidget::closeFile(int index)
{
	removeTab(index);
}

void TextEditorWidget::selectAll()
{
	int index = currentIndex();
	QPlainTextEdit* widget = dynamic_cast<QPlainTextEdit*>(this->widget(index));
	widget->selectAll();
}

void TextEditorWidget::cut()
{
	int index = currentIndex();
	QPlainTextEdit* widget = dynamic_cast<QPlainTextEdit*>(this->widget(index));
	widget->cut();
}

void TextEditorWidget::copy()
{
	int index = currentIndex();
	QPlainTextEdit* widget = dynamic_cast<QPlainTextEdit*>(this->widget(index));
	widget->copy();
}

void TextEditorWidget::paste()
{
	int index = currentIndex();
	QPlainTextEdit* widget = dynamic_cast<QPlainTextEdit*>(this->widget(index));
	widget->paste();
}

void TextEditorWidget::undo()
{
	int index = currentIndex();
	QPlainTextEdit* widget = dynamic_cast<QPlainTextEdit*>(this->widget(index));
	widget->undo();
}

void TextEditorWidget::redo()
{
	int index = currentIndex();
	QPlainTextEdit* widget = dynamic_cast<QPlainTextEdit*>(this->widget(index));
	widget->redo();
}


TextEditor::TextEditor(QWidget* parent) :
	QDialog(parent)
{
	m_vboxLayout = new QVBoxLayout(this);
	m_editorWidget = new TextEditorWidget(this);
	m_menuBar = new QMenuBar(this);
	m_mainToolBar = new QToolBar(this);
	m_menuFile = new QMenu(this);
	m_menuEdit = new QMenu(this);
	m_menuView = new QMenu(this);

	setMinimumSize(400, 400);
	m_mainToolBar->addAction("Open file");
	m_mainToolBar->addAction("Close current");
	m_mainToolBar->addAction("Save Current");
	m_mainToolBar->addSeparator();
	m_mainToolBar->addAction("Undo");
	m_mainToolBar->addAction("Redo");

	m_menuFile->addAction("Open file");
	m_menuFile->addSeparator();
	m_menuFile->addAction("Close current");
	m_menuFile->addAction("Save current");
	m_menuFile->addSeparator();
	m_menuFile->addAction("Close");

	m_menuEdit->addAction("Undo");
	m_menuEdit->addAction("Redo");
	m_menuEdit->addSeparator();
	m_menuEdit->addAction("Cut");
	m_menuEdit->addAction("Copy");
	m_menuEdit->addAction("Paste");
	m_menuEdit->addSeparator();
	m_menuEdit->addAction("Select all");

	m_menuView->addAction("Show fullscreen");
	m_menuView->addAction("Show normal");
	m_menuView->addSeparator();
	m_menuView->addAction("Show maximized");
	m_menuView->addAction("Show minimized");

	m_vboxLayout->addWidget(m_mainToolBar);
	m_vboxLayout->addWidget(m_editorWidget);

	m_mainToolBar->setMovable(true);
	m_mainToolBar->setFloatable(true);
	m_mainToolBar->setAllowedAreas(Qt::ToolBarArea::AllToolBarAreas);

	QList<QAction*> toolBarActions = m_mainToolBar->actions();

	QObject::connect(toolBarActions[0], &QAction::triggered, this, &TextEditor::openTextFile);
	QObject::connect(toolBarActions[1], &QAction::triggered, this, &TextEditor::closeTextFile);
	QObject::connect(toolBarActions[2], &QAction::triggered, this, &TextEditor::saveTextFile);

	QObject::connect(toolBarActions[4], &QAction::triggered, m_editorWidget, &TextEditorWidget::undo);
	QObject::connect(toolBarActions[5], &QAction::triggered, m_editorWidget, &TextEditorWidget::redo);

	setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint
								 & ~Qt::WindowMinimizeButtonHint
								 & ~Qt::WindowContextHelpButtonHint);

	//m_vboxLayout->setMenuBar(m_mainToolBar);
	setLayout(m_vboxLayout);
}

void TextEditor::openTextFile()
{
	QString filePath = QFileDialog::getOpenFileName(this, "Open file...");
	if(filePath.isEmpty())
		return;

	m_editorWidget->openFile(filePath);
}

void TextEditor::closeTextFile()
{
	int currentTabIndex = m_editorWidget->currentIndex();
	if(!(currentTabIndex >= 0)) return;

	QPlainTextEdit* textedit = dynamic_cast<QPlainTextEdit*>(m_editorWidget->widget(currentTabIndex));
	QTextDocument* document = textedit->document();

	if(document->isModified()) {

		QMessageBox* unsavedDocumentMsgBox = new QMessageBox(this);
		unsavedDocumentMsgBox->setText("Do you want to save changes?");
		unsavedDocumentMsgBox->setWindowTitle("Unsaved changes!");
		unsavedDocumentMsgBox->setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);

		int result = unsavedDocumentMsgBox->exec();
		if(result == QMessageBox::Save) {
			saveTextFile();
		}

		m_editorWidget->closeFile(currentTabIndex);
	} else m_editorWidget->closeFile(currentTabIndex);
}

void TextEditor::saveTextFile()
{
	int currentTabIndex = m_editorWidget->currentIndex();
	if(!(currentTabIndex >= 0)) {
		return;
	}

	m_editorWidget->saveFile(currentTabIndex);
}

TextEditor::~TextEditor()
{
}


AboutDialog::AboutDialog(QWidget* parent) : QDialog(parent)
{
	setWindowIcon(QIcon(QPixmap("resources/vum1.png")));

	m_layout = new QHBoxLayout(this);
	m_vumpicture = new QLabel(this);
	m_textlabel = new QLabel(this);

	m_vumpicture->setMaximumSize(QSize(134, 50));
	m_textlabel->setOpenExternalLinks(true);
	m_textlabel->setText("Written as a final year project"
				" for university.\n"
				"<a href=\"https://vum.bg/\">https://vum.bg</a>");

	QObject::connect(m_textlabel, &QLabel::linkActivated, this, [this](const QString& link) {
		Q_EMIT this->linkActivated(link);
	});

	m_vumpicture->setPixmap(QPixmap("resources/vum.png"));

	m_textlabel->setTextFormat(Qt::RichText);
	m_textlabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
	m_textlabel->setOpenExternalLinks(false);

	m_layout->addWidget(m_vumpicture);
	m_layout->addWidget(m_textlabel);
	setWindowTitle("About");
	setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint
								 & ~Qt::WindowMinimizeButtonHint
								 & ~Qt::WindowContextHelpButtonHint);
	setMaximumSize(width(), height());
	setModal(true);

	setLayout(m_layout);
}

DocumentationDialog::DocumentationDialog(QWidget* parent) :
	QDialog(parent)
{
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

	QHBoxLayout* layout = new QHBoxLayout(this);
	QWebEngineView* view = new QWebEngineView(this);
	view->load(QUrl::fromLocalFile(toAbsolutePath("resources/doxygen/html/index.html")));
	layout->addWidget(view);
	setMinimumSize(QSize(1200, 600));
	setWindowTitle("Documentation");
	setLayout(layout);
}

SpecificationsDialog::SpecificationsDialog(QWidget* parent) :
	QDialog(parent)
{
	using namespace core::system;

	m_layout = new QHBoxLayout(this);
	m_textlabel = new QLabel(this);

	CPUFeatures features;
	SystemInfo system;

	QString RAM = QString("RAM : ") + QString::number(system.RAMGigaBytes()) + QString("GB ") + QString("(") + QString::number(system.RAMKiloBytes()) + QString(" KB)") + "\n";
	QString CPU = QString("CPU : ") + system.brandName() + "\n";
	QString vendor = QString("Vendor : ") + system.vendorName() + "\n";
	QString logicalcores = "Logical cores : " + QString::number(system.logicalCores()) + "\n";
	QString cores = "Cores : " + QString::number(system.cores()) + "\n";
	QString pagesize = QString("Page size : ") + QString::number(system.pageSize()) + "\n";
	QString httsupported = "Hyper Threading : " + features.isHyperThreadingSupported() + "\n";
	QString cache = "\n\nCache information : \n";
	QString l1 = "L1 : " + QString("Size (") + QString::number(system.cache(1).size) + ") Line size (" + QString::number(system.cache(1).line_size) + ") Associativity (" + QString::number(system.cache(1).associativity) + ")\n";
	QString l2 = "L2 : " + QString("Size (") + QString::number(system.cache(2).size) + ") Line size (" + QString::number(system.cache(2).line_size) + ") Associativity (" + QString::number(system.cache(2).associativity) + ")\n";
	QString l3 = "L3 : " + QString("Size (") + QString::number(system.cache(3).size) + ") Line size (" + QString::number(system.cache(3).line_size) + ") Associativity (" + QString::number(system.cache(3).associativity) + ")\n";

	QString feat = "\n\nFeatures supported : \n";
	QString pclmulqdq = "PCLMULQDQ : " + features.isPCLMULQDQSupported() + "\n";
	QString fpu = "FPU : " + features.isFPUSupported() + "\n";
	QString AVX = "AVX : " + features.isAVXSupported() + "\n";
	QString MMX = "MMX : " + features.isMMXSupported() + "\n";
	QString SSE = "SSE : " + features.isSSESupported() + "\n";
	QString AVX2 = "AVX2 : " + features.isAVX2Supported() + "\n";
	QString SSE2 = "SSE2 : " + features.isSSE2Supported() + "\n";
	QString SSE3 = "SSE3 : " + features.isSSE3Supported() + "\n";
	QString SSE41 = "SSE4.1 : " + features.isSSE41Supported() + "\n";
	QString SSE42 = "SSE4.2 : " + features.isSSE42Supported() + "\n";

	m_textlabel->setText(RAM + CPU + vendor + cores + logicalcores + pagesize + cache + l1 + l2 + l3 + feat + httsupported + SSE + SSE2 + SSE3 + SSE41 + SSE42 + MMX + AVX + AVX2 + pclmulqdq + fpu);

	QObject::connect(m_textlabel, &QLabel::linkActivated, this, [this](const QString& link) {
		Q_EMIT this->linkActivated(link);
	});

	m_textlabel->setOpenExternalLinks(false);
	m_layout->addWidget(m_textlabel);
	setWindowTitle("Hardware Specifications");
	setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint
								 & ~Qt::WindowMinimizeButtonHint
								 & ~Qt::WindowContextHelpButtonHint);

	setMaximumSize(width(), height());
	setModal(true);

	setLayout(m_layout);
	setWindowIcon(QIcon("resources/chip.png"));
}

/*
ArchiverWidget::ArchiverWidget(QWidget* parent) : QDialog(parent)
{
	m_buttonsLayout = new QHBoxLayout(this);
	m_widgetsLayout = new QVBoxLayout(this);
	m_openArchive = new QPushButton("Open...");
	m_extractButton = new QPushButton("Extract", this);
	m_archiveDetailsButton = new QPushButton("Archive details", this);
	m_model = new QFileSystemModel(this);
	m_treeWidget = new QTreeView(this);
	m_treeWidget->setModel(m_model);

	m_buttonsLayout->addWidget(m_openArchive);
	m_buttonsLayout->addWidget(m_extractButton);
	m_buttonsLayout->addWidget(m_archiveDetailsButton);

	QWidget* w = new QWidget(this);
	w->setLayout(m_buttonsLayout);
	m_widgetsLayout->addWidget(w);
	m_widgetsLayout->addWidget(m_treeWidget);
	setLayout(m_widgetsLayout);
	setMinimumSize(600, 400);

	QObject::connect(m_openArchive, &QPushButton::clicked, this, &ArchiverWidget::openArchiveFile);
	QObject::connect(m_extractButton, &QPushButton::clicked, this, &ArchiverWidget::extractArchiveFile);
}


void ArchiverWidget::openArchiveFile()
{
	QString path = QFileDialog::getOpenFileName();
	if(path.isEmpty()) return;

	core::archive::Archive archive(path);
	archive.setExtractionMethod(core::archive::Archive::TypeOfArchiveData::HeaderData);

	QDir header_dir("header-data");
	if(header_dir.exists())
		header_dir.removeRecursively();

	header_dir.mkdir("header-data");
	archive.extractTo(header_dir.path());
	m_model->setFilter(QDir::NoDotAndDotDot | QDir::Dirs);
	m_treeWidget->setCurrentIndex(m_model->index("header-data"));

}

void ArchiverWidget::extractArchiveFile()
{

}
*/
