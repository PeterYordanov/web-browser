#ifndef AVER_WEBENGINE_VIEW_HPP
#define AVER_WEBENGINE_VIEW_HPP

#include <QWebEnginePage>
#include <QWebEngineView>

QT_BEGIN_NAMESPACE
class QWidget;
class QContextMenuEvent;
class QMenu;
QT_END_NAMESPACE

class WebEngineView : public QWebEngineView
{
	Q_OBJECT
public:
	explicit WebEngineView(QWidget* parent = nullptr);

public:
#ifndef QT_NO_CONTEXTMENU
	void contextMenuEvent(QContextMenuEvent*) Q_DECL_OVERRIDE;

private:
	QMenu* m_menu;
#endif // QT_NO_CONTEXTMENU

};


class WebEnginePage : public QWebEnginePage
{
	explicit WebEnginePage(QWidget* parent = nullptr);
};

#endif // AVER_WEBENGINE_VIEW_HPP
