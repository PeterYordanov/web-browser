#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

QT_BEGIN_NAMESPACE
class QWebEngineView;
class QLineEdit;
class QTabWidget;
class QWebEngineProfile;
class QPushButton;
class QToolBar;
class QMenuBar;
class QMenu;
class QAction;
class QLabel;
class QHBoxLayout;
class QVBoxLayout;
class QProgressBar;
class QCompleter;
class QComboBox;
QT_END_NAMESPACE

class TabWidget;
class DownloadManagerDialog;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget* parent = nullptr);

private:
	void _allocateResources();
	void _setUpUI();
	void _createMenuBar();
	void _createFileMenu();
	void _createViewMenu();
	void _createWindowMenu();
	void _createHistoryMenu();
	void _createHelpMenu();

private:
	QLineEdit* m_url = nullptr;
	QLineEdit* m_search = nullptr;
	QWebEngineProfile* m_profile = nullptr;
	QToolBar* m_toolBar = nullptr;
	QMenuBar* m_menuBar = nullptr;
	QMenu* m_fileMenu = nullptr;
	QMenu* m_viewMenu = nullptr;
	QMenu* m_windowMenu = nullptr;
	QMenu* m_historyMenu = nullptr;
	QMenu* m_helpMenu = nullptr;
	QVBoxLayout* m_verticalLayout = nullptr;
	QHBoxLayout* m_horizontalLayout = nullptr;
	QAction* m_goForward = nullptr;
	QAction* m_goBack = nullptr;
	QAction* m_reload = nullptr;
	QAction* m_downloadManagerButton = nullptr;
	TabWidget* m_tabWidget = nullptr;
	QProgressBar* m_progressBar = nullptr;
	QHBoxLayout* m_toolBarLayout = nullptr;
	DownloadManagerDialog* m_downloadManagerDialog = nullptr;
	QWidget* m_widgetConverter = nullptr;
	QLabel* m_searchIcon = nullptr;
	QComboBox* m_searchComboBox = nullptr;
	QCompleter* m_completer;
};

#endif // MAINWINDOW_HPP
