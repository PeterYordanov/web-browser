#ifndef AVER_TAB_WIDGET_HPP
#define AVER_TAB_WIDGET_HPP

#include <QTabWidget>
#include <QWebEngineProfile>

QT_BEGIN_NAMESPACE
class QProgressBar;
class QContextMenuEvent;
QT_END_NAMESPACE

class LineEdit;
class WebEngineView;
class chip8;
class WebEnginePage;

class TabWidget : public QTabWidget
{
	Q_OBJECT

public:
	explicit TabWidget(QWebEngineProfile* profile, QProgressBar*, QWidget* parent = nullptr);
	WebEngineView* webEngineView();

public Q_SLOTS:
	void loadForCurrentTab(const QUrl&);
	void nextTab();
	void previousTab();
	void createTab();
	void closeCurrentTab();
	void closeTab(int);
	void goBack();
	void goForward();
	void reloadCurrent();
	void handleLoadFinished(bool);

#ifndef QT_NO_CONTEXTMENU
	void contextMenuEvent(QContextMenuEvent*);
#endif

	void mouseMoveEvent(QMouseEvent*);

Q_SIGNALS:
	void urlChanged(const QString&);
	void titleChanged(const QString&);
	void iconChanged(const QIcon&);
	void linkHovered(const QString&);

private:
	QProgressBar* m_progressbar;
};


#endif // AVER_WIDGETS_HPP
