#include "aver_download_manager.hpp"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QProgressBar>
#include <QScrollBar>
#include <QScrollArea>
#include <QLabel>
#include <QWidget>
#include <QString>
#include <QWebEngineDownloadItem>
#include <QTimer>
#include <QTime>
#include <QFileInfo>
#include <QUrl>
#include <QFileDialog>
#include "core/av_core.hpp"

DownloadManagerItem::DownloadManagerItem(QWebEngineDownloadItem* downloadItem, QWidget* parent) :
	QFrame(parent),
	m_timeElapsed(QTime::currentTime()),
	m_downloadItem(downloadItem)
{
	m_hboxLayout = new QHBoxLayout(this);
	m_fileName = new QLabel(this);
	m_cancel = new QPushButton(this);
	m_cancel->setIcon(QIcon("resources/stop.png"));
	m_progressBar = new QProgressBar(this);
	m_scrollBar = new QScrollBar(this);
	m_scrollArea = new QScrollArea(this);

	m_progressBar->hide();
	m_hboxLayout->addWidget(m_fileName);
	m_hboxLayout->addWidget(m_cancel);

	m_scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOn);
	m_scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAsNeeded);
	m_scrollBar->hide();
	m_scrollArea->hide();

	m_fileName->setText(QFileInfo(m_downloadItem->path()).fileName());

	QObject::connect(m_cancel, &QPushButton::clicked, this, [this](bool) {
		if(m_downloadItem->state() == QWebEngineDownloadItem::DownloadInProgress)
			m_downloadItem->cancel();
		else Q_EMIT removeClicked(this);
	});

	QObject::connect(m_downloadItem, &QWebEngineDownloadItem::downloadProgress, this, &DownloadManagerItem::updateWidget);
	QObject::connect(m_downloadItem, &QWebEngineDownloadItem::stateChanged, this, &DownloadManagerItem::updateWidget);

	//updateWidget();

	m_hboxLayout->addWidget(m_fileName);
	m_hboxLayout->addWidget(m_cancel);
	setLayout(m_hboxLayout);
}


/* Heavily inspired by :
 * https://bit.ly/2Auk5aX
 */
void DownloadManagerItem::updateWidget()
{
	auto unit = [](double bytes) {
		if (bytes < (1 << 10))
			return QString::number(bytes) + " B";
		else if (bytes < (1 << 20))
			return QString::number(bytes / (1 << 10)) + " KiB";
		else if (bytes < (1 << 30))
			return QString::number(bytes / (1 << 20)) + " MiB";
		else
			return QString::number(bytes / (1 << 30)) + " GiB";
	};

	double totalBytes = m_downloadItem->totalBytes();
	double receivedBytes = m_downloadItem->receivedBytes();
	double bytesPerSecond = receivedBytes / m_timeElapsed.elapsed() * 1000;

	switch (m_downloadItem->state()) {
	case QWebEngineDownloadItem::DownloadRequested:
		Q_UNREACHABLE();
		break;
	case QWebEngineDownloadItem::DownloadInProgress:
		if (totalBytes >= 0) {
			m_progressBar->setValue(qRound(100 * receivedBytes / totalBytes));
			m_progressBar->setDisabled(false);
			m_progressBar->setFormat(
				QString("%p% - %1 of %2 downloaded - %3/s")
					.arg(unit(receivedBytes))
					.arg(unit(totalBytes))
					.arg(unit(bytesPerSecond)));
		} else {
			m_progressBar->setValue(0);
			m_progressBar->setDisabled(false);
			m_progressBar->setFormat(
				QString("unknown size - %1 downloaded - %2/s").arg(unit(receivedBytes)).arg(unit(bytesPerSecond)));
		}
		break;
	case QWebEngineDownloadItem::DownloadCompleted:
		m_progressBar->setValue(100);
		m_progressBar->setDisabled(true);
		m_progressBar->setFormat(
			QString("completed - %1 downloaded - %2/s").arg(unit(receivedBytes)).arg(unit(bytesPerSecond)));
		break;
	case QWebEngineDownloadItem::DownloadCancelled:
		m_progressBar->setValue(0);
		m_progressBar->setDisabled(true);
		m_progressBar->setFormat(
		QString("cancelled - %1 downloaded - %2/s").arg(unit(receivedBytes)).arg(unit(bytesPerSecond)));
		break;
	case QWebEngineDownloadItem::DownloadInterrupted:
		m_progressBar->setValue(0);
		m_progressBar->setDisabled(true);
		m_progressBar->setFormat(
		QString("interrupted: %1").arg(m_downloadItem->interruptReasonString()));
		break;
	}

	if (m_downloadItem->state() == QWebEngineDownloadItem::DownloadInProgress) {
		m_cancel->setIcon(QIcon("resources/stop.png"));
		m_cancel->setToolTip("Stop downloading");
	} else {
		m_cancel->setIcon(QIcon("resources/clear.png"));
		m_cancel->setToolTip("Remove from list");
	}
}

DownloadManagerDialog::DownloadManagerDialog(QWidget* parent) :
	QDialog(parent),
	m_layout(nullptr),
	m_scrollarea(nullptr),
	m_scrollbar(nullptr),
	m_downloads(0)
{
	m_layout = new QVBoxLayout(this);
	setMinimumSize(QSize(320, 200));
	setWindowTitle("Download Manager");
	setWindowFlags(windowFlags() & ~Qt::WindowMinimizeButtonHint);
	setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	setWindowIcon(QIcon("resources/download.png"));
	setLayout(m_layout);
}

void DownloadManagerDialog::addItem(DownloadManagerItem* item)
{
	QObject::connect(item, &DownloadManagerItem::removeClicked, this, &DownloadManagerDialog::removeItem);
	m_layout->addWidget(item);
}

void DownloadManagerDialog::removeItem(DownloadManagerItem* item)
{
	delete item;
	m_downloads--;
}

void DownloadManagerDialog::download(QWebEngineDownloadItem* item)
{
	QString url = item->url().toString();
	QString saveFilePath = QFileDialog::getSaveFileName(this, "Save file...", url.split('/').last(),
																			url.split(".").last().toUpper());
	if(!saveFilePath.isEmpty()) {
		item->setPath(saveFilePath);
		item->accept();
		addItem(new DownloadManagerItem(item));
	}
}
