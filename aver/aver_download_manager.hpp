#ifndef DOWNLOADMANAGER_HPP
#define DOWNLOADMANAGER_HPP

#include <QFrame>
#include <QDialog>
#include <QTime>

QT_BEGIN_NAMESPACE
class QHBoxLayout;
class QVBoxLayout;
class QPushButton;
class QScrollBar;
class QScrollArea;
class QProgressBar;
class QLabel;
class QWebEngineDownloadItem;
QT_END_NAMESPACE

class DownloadManagerItem : public QFrame
{
	Q_OBJECT

public:
	explicit DownloadManagerItem(QWebEngineDownloadItem*,QWidget* parent = nullptr);

Q_SIGNALS:
	void removeClicked(DownloadManagerItem*);

public Q_SLOTS:
	void updateWidget();

private:
	QProgressBar* m_progressBar;
	QLabel* m_fileName;
	QHBoxLayout* m_hboxLayout;
	QPushButton* m_cancel;
	QTime m_timeElapsed;
	QWebEngineDownloadItem* m_downloadItem;
	QScrollBar* m_scrollBar;
	QScrollArea* m_scrollArea;
};

class DownloadManagerDialog : public QDialog
{
	Q_OBJECT

public:
	explicit DownloadManagerDialog(QWidget* parent = nullptr);

	void addItem(DownloadManagerItem*);
	void removeItem(DownloadManagerItem*);
	void download(QWebEngineDownloadItem*);

private:
	QVBoxLayout* m_layout;
	QScrollArea* m_scrollarea;
	QScrollBar* m_scrollbar;
	int m_downloads;
};

#endif // DOWNLOADMANAGER_HPP
