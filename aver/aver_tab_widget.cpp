#include "aver_tab_widget.hpp"

#include <QWidget>
#include <QContextMenuEvent>
#include <QMenu>
#include <QTimer>
#include <QMessageBox>
#include <QMainWindow>
#include <QTabBar>
#include <QUrl>
#include <QProgressBar>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileInfo>
#include <QTreeWidget>
#include <QMessageBox>
#include <QFileDialog>
#include <QString>
#include <QPlainTextEdit>
#include <QFileSystemModel>
#include <QPlainTextEdit>
#include <QTimer>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDir>
#include <QFileSystemModel>
#include <QTreeView>
#include "aver/aver_webengine.hpp"
#include "core/av_core.hpp"
#include "aver_webengine.hpp"
#include AV_C_API_DEBUG
//#include AV_CPP_API_ARCHIVER
#include AV_CPP_API_SYNTHESIZER

TabWidget::TabWidget(QWebEngineProfile* profile, QProgressBar* progressbar, QWidget* parent) : QTabWidget (parent)
{
	(void)profile;
	if(progressbar != nullptr)
		m_progressbar = progressbar;

	setDocumentMode(true);
	setElideMode(Qt::ElideRight);

	QTabBar* tabbar = this->tabBar();
	tabbar->setTabsClosable(true);
	tabbar->setSelectionBehaviorOnRemove(QTabBar::SelectPreviousTab);
	tabbar->setMovable(true);
	tabbar->setContextMenuPolicy(Qt::CustomContextMenu);

	QString empty_page = QUrl::fromLocalFile(toAbsolutePath("resources/home-page.html")).toString();
	loadForCurrentTab(empty_page);

	QObject::connect(tabbar, &QTabBar::tabCloseRequested, this, &TabWidget::closeTab);
	//QObject::connect(tabbar, &QTabBar::tabBarDoubleClicked, this, &TabWidget::closeTab);
}

WebEngineView* TabWidget::webEngineView()
{
	return dynamic_cast<WebEngineView*>(widget(currentIndex()));
}

void TabWidget::nextTab()
{
	if(!(currentIndex() == count() - 1))
		setCurrentIndex(currentIndex() + 1);
	else if(currentIndex() == count() - 1)
		setCurrentIndex(0);
}

void TabWidget::previousTab()
{
	if(!(currentIndex() == 0))
		setCurrentIndex(currentIndex() - 1);
	else if(currentIndex() == 0)
		setCurrentIndex(count() - 1);
}

void TabWidget::createTab()
{
	WebEngineView* view = new WebEngineView(this);
	addTab(view, "New tab...");
	setCurrentIndex(count() - 1);
	view->load(QUrl::fromLocalFile(toAbsolutePath("resources/home-page.html")));

	QObject::connect(this->webEngineView()->page(), &WebEnginePage::selectionChanged, this, [this]() {
		using namespace core::synth;

		QString text = this->webEngineView()->page()->selectedText();
		if(!text.isNull() && !text.isEmpty() && text.isSimpleText()) {
			AbstractSynthesizer* synth = SynthesizerFactory::createSynthesizer(core::synth::SynthesizerFactory::Native, this->webEngineView());
			synth->setGender(AbstractSynthesizer::MALE);
			synth->synthesize(text.toStdString().c_str());
			av_debugf(text.toStdString().c_str());
		}
	});

	QObject::connect(view, &WebEngineView::urlChanged, this, [this](const QUrl& url) {
		Q_EMIT this->urlChanged(url.toString());
	});

	QObject::connect(view, &WebEngineView::titleChanged, this, [this](const QString& title) {
		this->setTabText(currentIndex(), title);
		Q_EMIT titleChanged(title);
	});

	QObject::connect(view, &WebEngineView::iconChanged, this, [this](const QIcon& icon) {
		this->setTabIcon(currentIndex(), icon);
		Q_EMIT iconChanged(icon);
	});
}

void TabWidget::closeCurrentTab()
{
	delete webEngineView();
}

void TabWidget::loadForCurrentTab(const QUrl& url)
{
	if(currentIndex() < 0)
		createTab();

	WebEngineView* view = nullptr;
	if(!url.toString().isEmpty()) {
		view = dynamic_cast<WebEngineView*>(widget(currentIndex()));
		QObject::connect(view, &WebEngineView::loadProgress, m_progressbar, [this](int progress) {
			m_progressbar->show();
			m_progressbar->setValue(progress);
			if(progress == 100) {
				m_progressbar->setValue(0);
				m_progressbar->hide();
			}
		});


		QObject::connect(view->page(), &WebEnginePage::loadFinished, this, &TabWidget::handleLoadFinished);

		view->load(url);

		QObject::connect(view->page(), &WebEnginePage::linkHovered, this, [this](const QUrl& url) {
			Q_EMIT linkHovered(url.toString());
		});
	}
}


void TabWidget::closeTab(int index)
{
	delete dynamic_cast<WebEngineView*>(widget(index));
	if(count() == 0)
		loadForCurrentTab(QUrl::fromLocalFile(toAbsolutePath("resources/home-page.html")).toString());
}

void TabWidget::goBack()
{
	if(currentIndex() >= 0 && currentIndex() <= count() - 1) {
		WebEngineView* view = dynamic_cast<WebEngineView*>(widget(currentIndex()));
		view->back();
	}
}

void TabWidget::goForward()
{
	if(currentIndex() >= 0 && currentIndex() <= count() - 1) {
		WebEngineView* view = dynamic_cast<WebEngineView*>(widget(currentIndex()));
		view->forward();
	}
}

void TabWidget::reloadCurrent()
{
	dynamic_cast<WebEngineView*>(widget(currentIndex()))->reload();
}


#ifndef QT_NO_CONTEXTMENU
void TabWidget::contextMenuEvent(QContextMenuEvent* ev)
{
	QMenu* menu = new QMenu(this);
	menu->addAction("New tab");
	menu->addAction("Close tab");
	menu->addSeparator();
	menu->addAction("Reload");
	menu->addAction("Reload all tabs");
	menu->addSeparator();
	menu->addAction("Forward");
	menu->addAction("Back");
	menu->addSeparator();
	menu->addAction("Next tab");
	menu->addAction("Previous tab");
	menu->addAction("Say");

	QList<QAction*> actions = menu->actions();

	QObject::connect(actions[0], &QAction::triggered, this, &TabWidget::createTab);
	QObject::connect(actions[1], &QAction::triggered, this, &TabWidget::closeCurrentTab);
	QObject::connect(actions[3], &QAction::triggered, this, &TabWidget::reloadCurrent);
	QObject::connect(actions[4], &QAction::triggered, this, [this](){
		int currentIndex = this->currentIndex();
		for (int i = 0; i < this->count(); i++) {
			this->setCurrentIndex(i);
			this->reloadCurrent();
		}

		this->setCurrentIndex(currentIndex);
	});

	QObject::connect(actions[6], &QAction::triggered, this, [this]() {
		this->goForward();
	});

	QObject::connect(actions[7], &QAction::triggered, this, [this]() {
		this->goBack();
	});

	QObject::connect(actions[9], &QAction::triggered, this, [this]() {
		this->nextTab();
	});

	QObject::connect(actions[10], &QAction::triggered, this, [this]() {
		this->previousTab();
	});

	menu->popup(ev->globalPos());
}
#endif

//For some reason, it crashes as a lambda :/
void TabWidget::handleLoadFinished(bool ok)
{
	if(!ok) {
		QDir dir(AV_RESOURCES_PATH_RELATIVE);
		webEngineView()->load(QUrl::fromLocalFile(toAbsolutePath("resources/pagenotloaded.html")));
		setTabText(currentIndex(), "Error!");
	}
}

void TabWidget::mouseMoveEvent(QMouseEvent* /*event*/)
{
	av_debugf("Mousemove\n");
}

