#ifndef AVER_DIALOGS
#define AVER_DIALOGS

#include <QWidget>
#include <QSyntaxHighlighter>
#include <QTabWidget>

QT_BEGIN_NAMESPACE
class QStatusBar;
class QToolBar;
class QMenu;
class QMenuBar;
class QVBoxLayout;
QT_END_NAMESPACE

class AbstractHighlighter : public QSyntaxHighlighter
{

};

class TextEditorWidget : public QTabWidget
{
	Q_OBJECT
public:
	explicit TextEditorWidget(QWidget* parent = nullptr);

public Q_SLOTS:
	void openFile(const QString& path);
	void saveFile(int index);
	void closeFile(int index);

	void selectAll();
	void cut();
	void copy();
	void paste();
	void redo();
	void undo();

private:
	QVBoxLayout*		m_vboxLayout;
	QList<QStringList>  m_textList;
	int					m_newTabs;
};

#include <QDialog>

QT_BEGIN_NAMESPACE
class QTabWidget;
class QStatusBar;
class QToolBar;
class QMenu;
class QMenuBar;
class QVBoxLayout;
QT_END_NAMESPACE
class TextEditorWidget;

class TextEditor : public QDialog
{
	Q_OBJECT
public:
	explicit TextEditor(QWidget* parent = Q_NULLPTR);
	~TextEditor();

private Q_SLOTS:
	void openTextFile();
	void closeTextFile();
	void saveTextFile();

private:
	TextEditorWidget* m_editorWidget;
	QMenuBar* m_menuBar;
	QMenu* m_menuFile;
	QMenu* m_menuEdit;
	QMenu* m_menuView;
	QStatusBar* m_statusBar;
	QToolBar* m_mainToolBar;
	QVBoxLayout* m_vboxLayout;
};

QT_BEGIN_NAMESPACE
class QHBoxLayout;
class QLabel;
class QPushButton;
QT_END_NAMESPACE

class AboutDialog : public QDialog
{
	Q_OBJECT

public:
	explicit AboutDialog(QWidget* parent = nullptr);

Q_SIGNALS:
	void linkActivated(const QString&);

private:
	QHBoxLayout* m_layout;
	QLabel* m_textlabel;
	QLabel* m_vumpicture;
};

class DocumentationDialog : public QDialog
{
	Q_OBJECT

public:
	explicit DocumentationDialog(QWidget* parent = nullptr);
};

class SpecificationsDialog : public QDialog
{
	Q_OBJECT
public:
	explicit SpecificationsDialog(QWidget* parent = nullptr);

Q_SIGNALS:
	void linkActivated(const QString&);

private:
	QHBoxLayout* m_layout;
	QLabel* m_textlabel;
};

QT_BEGIN_NAMESPACE
class QHBoxLayout;
class QVBoxLayout;
class QTreeView;
class QFileSystemModel;
class QPushButton;
QT_END_NAMESPACE

/*
class ArchiverWidget : public QDialog
{
	Q_OBJECT
public:
	explicit ArchiverWidget(QWidget* parent = nullptr);

public Q_SLOTS:
	void openArchiveFile();
	void extractArchiveFile();

private:
	QHBoxLayout* m_buttonsLayout;
	QVBoxLayout* m_widgetsLayout;
	QPushButton* m_extractButton;
	QPushButton* m_archiveDetailsButton;
	QPushButton* m_openArchive;
	QTreeView* m_treeWidget;
	QFileSystemModel* m_model;
};*/

#endif // AVER_DIALOGS
