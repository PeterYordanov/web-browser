#include "aver.hpp"
#include <QWebEngineView>
#include <QTabWidget>
#include <QWebEngineProfile>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QtWebEngine>
#include <QHBoxLayout>
#include <QPushButton>
#include <QToolBar>
#include <QAction>
#include <QMenu>
#include <QMenuBar>
#include <QSize>
#include <QRect>
#include <QApplication>
#include <QDesktopWidget>
#include <QList>
#include <QKeySequence>
#include <QLabel>
#include <QHBoxLayout>
#include <QProgressBar>
#include "aver_dialogs.hpp"
#include "aver_tab_widget.hpp"
#include "aver_download_manager.hpp"
#include <QComboBox>
#include <QCompleter>
#include "core/av_core.hpp"
#include <QStatusBar>
#include AV_C_API_UTILITIES

MainWindow::MainWindow(QWidget* parent)
	: QMainWindow(parent)
{
	QtWebEngine::initialize();
	_allocateResources();
	_createMenuBar();
	_createFileMenu();
	_createViewMenu();
	_createWindowMenu();
	_createHistoryMenu();
	_createHelpMenu();
	_setUpUI();
	setCentralWidget(m_widgetConverter);
}

void MainWindow::_setUpUI()
{
	setMinimumSize(900, 500);
	showMaximized();

	m_url->setCompleter(m_completer);
	m_searchComboBox->addItem(QIcon("resources/google_icon.png"), "Google");
	m_searchComboBox->addItem(QIcon("resources/duckduckgo_icon.png"), "DuckDuckGo");
	m_searchComboBox->addItem(QIcon("resources/bing_icon.png"), "Bing");
	m_searchComboBox->addItem(QIcon("resources/yahoo_icon.png"), "Yahoo");
	m_searchComboBox->setCurrentText("Google");

	m_goForward->setIcon(QIcon("resources/forward.png"));
	m_goBack->setIcon(QIcon("resources/backward.png"));
	m_reload->setIcon(QIcon("resources/reload.png"));
	m_downloadManagerButton->setIcon(QIcon("resources/download.png"));
	m_progressBar->hide();
	m_progressBar->setMaximumHeight(3);
	m_progressBar->setTextVisible(false);
	m_progressBar->setStyleSheet(QStringLiteral("QProgressBar::chunk { background-color: #f44336 }"));

	m_searchIcon->setPixmap(QPixmap("resources/search.png"));
	m_searchIcon->setStyleSheet("QLineEdit { background-color: white }");
	m_toolBar->addAction(m_goBack);
	m_toolBar->addAction(m_goForward);
	m_toolBar->addAction(m_reload);

	m_url->setClearButtonEnabled(true);
	m_url->addAction(QIcon("resources/search.png"), QLineEdit::LeadingPosition);
	m_url->setPlaceholderText("Type an URL");
	m_search->setPlaceholderText("Google Search");
	m_search->setMaximumWidth(200);
	m_search->setClearButtonEnabled(true);


	m_toolBar->addWidget(m_url);
	m_toolBar->addWidget(m_searchComboBox);
	m_toolBar->addWidget(m_search);
	m_toolBar->addAction(m_downloadManagerButton);
	m_horizontalLayout->addWidget(m_toolBar);

	m_verticalLayout->addLayout(m_horizontalLayout);
	m_verticalLayout->addWidget(m_progressBar);
	m_verticalLayout->addWidget(m_tabWidget);

	m_widgetConverter->setLayout(m_verticalLayout);

	QObject::connect(m_url, &QLineEdit::returnPressed, this, [this]() {
		if(!m_url->text().startsWith("https://"))
			m_url->setText("https://" + m_url->text());

		m_tabWidget->loadForCurrentTab(m_url->text());
		m_url->setFocus(Qt::ShortcutFocusReason);
	});

	QObject::connect(m_goBack, &QAction::triggered, m_tabWidget, &TabWidget::goBack);
	QObject::connect(m_goForward, &QAction::triggered, m_tabWidget, &TabWidget::goForward);
	QObject::connect(m_reload, &QAction::triggered, m_tabWidget, &TabWidget::reloadCurrent);

	QObject::connect(m_downloadManagerButton, &QAction::triggered, this, [this]() {
		m_downloadManagerDialog->show();
		m_downloadManagerDialog->raise();
		m_downloadManagerDialog->activateWindow();
	});

	QObject::connect(m_searchComboBox, QOverload<const QString&>::of(&QComboBox::activated), m_search, [&](const QString& text) {
		if(text == "Google")
			m_search->setPlaceholderText("Google Search");
		else if(text == "DuckDuckGo")
			m_search->setPlaceholderText("DuckDuckGo Search");
		else if(text == "Bing")
			m_search->setPlaceholderText("Bing search");
		else if(text == "Yahoo")
			m_search->setPlaceholderText("Yahoo search");
	});

	QObject::connect(m_search, &QLineEdit::returnPressed, this, [this]() {
		auto setLink = [this](QString text, QString separator){
			QString private_link = text;
			private_link += m_search->text().replace(' ', separator);
			m_search->setToolTip(private_link);
		};

		if(this->m_searchComboBox->currentText() == "Google") {
			setLink("https://www.google.com/search?q=", "+");
		} else if(this->m_searchComboBox->currentText() == "DuckDuckGo") {
			setLink("https://www.duckduckgo.com/", "%20");
		} else if(this->m_searchComboBox->currentText() == "Bing") {
			setLink("https://www.bing.com/search?q=", "+");
		} else if(this->m_searchComboBox->currentText() == "Yahoo") {
			setLink("https://search.yahoo.com/search?p=", "+");
		}

		m_url->setToolTip(m_search->toolTip());
		m_url->setText(m_url->toolTip());
		m_tabWidget->loadForCurrentTab(m_search->toolTip());
	});

	QObject::connect(QWebEngineProfile::defaultProfile(), &QWebEngineProfile::downloadRequested,
					 m_downloadManagerDialog, &DownloadManagerDialog::download);

	QObject::connect(m_tabWidget, &TabWidget::titleChanged, this, [this](const QString& title) {
		this->setWindowTitle(title);
	});

	QObject::connect(m_tabWidget, &TabWidget::iconChanged, this, [this](const QIcon& icon) {
		this->setWindowIcon(icon);
	});

	QObject::connect(m_tabWidget, &TabWidget::urlChanged, this, [this](const QString& url) {
		this->m_url->setText(url);
		if(this->m_url->text().contains("file"))
			this->m_url->clear();
	});

	QObject::connect(m_tabWidget, &TabWidget::linkHovered, this, [this](const QString& link) {
		this->statusBar()->showMessage(link);
	});
}

void MainWindow::_allocateResources()
{
	m_verticalLayout = new QVBoxLayout(this);
	m_horizontalLayout = new QHBoxLayout(this);
	m_toolBar = new QToolBar(this);
	m_profile = new QWebEngineProfile(this);
	m_url = new QLineEdit(this);
	m_progressBar = new QProgressBar(this);
	m_tabWidget = new TabWidget(m_profile, m_progressBar, this);
	m_widgetConverter = new QWidget(this);
	m_goForward = new QAction(this);
	m_goBack = new QAction(this);
	m_reload = new QAction(this);
	m_search = new QLineEdit(this);
	m_toolBarLayout = new QHBoxLayout(this);
	m_searchComboBox = new QComboBox(this);
	m_searchIcon = new QLabel(this);
	m_downloadManagerButton = new QAction(this);
	m_downloadManagerDialog = new DownloadManagerDialog(this);

	m_completer = new QCompleter(QStringList() << "google.com" << "duckduckgo.com"
								 << "bing.com" << "yahoo.com" << "vum.bg" << "facebook.com"
								 << "youtube.com" << "twitter.com" << "gitlab.com" << "github.com" << "goodreads.com"
								 << "amazon.com" << "wikipedia.com" << "instagram.com" << "yandex.ru" << "yandex.com"
								 << "netflix.com" << "twitch.tv" << "linkedin.com" << "stackoverflow.com", this);
}

void MainWindow::_createMenuBar()
{
	m_fileMenu = new QMenu(this);
	m_viewMenu = new QMenu(this);
	m_windowMenu = new QMenu(this);
	m_historyMenu = new QMenu(this);
	m_helpMenu = new QMenu(this);

	m_fileMenu->setTitle("File");
	m_viewMenu->setTitle("View");
	m_windowMenu->setTitle("Window");
	m_historyMenu->setTitle("History");
	m_helpMenu->setTitle("Help");

	m_menuBar = new QMenuBar(this);
	m_menuBar->addMenu(m_fileMenu);
	m_menuBar->addMenu(m_viewMenu);
	m_menuBar->addMenu(m_windowMenu);
	m_menuBar->addMenu(m_historyMenu);
	m_menuBar->addMenu(m_helpMenu);
	setMenuBar(m_menuBar);
}

void MainWindow::_createFileMenu()
{
	m_fileMenu->addAction("New Window");
	m_fileMenu->addSeparator();
	m_fileMenu->addAction("New Tab");
	m_fileMenu->addAction("Close Tab");
	m_fileMenu->addAction("Exit");

	QList<QAction*> actions = m_fileMenu->actions();
	actions[0]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_N));
	//Separator
	actions[2]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_T));
	actions[3]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_C));
	actions[4]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_Escape));

	QObject::connect(actions[0], &QAction::triggered, this, [&](){
		MainWindow* win = new MainWindow(this);
		win->show();
	});

	//Separator
	QObject::connect(actions[2], &QAction::triggered, m_tabWidget, &TabWidget::createTab);
	QObject::connect(actions[3], &QAction::triggered, m_tabWidget, &TabWidget::closeCurrentTab);
	QObject::connect(actions[4], &QAction::triggered, this, &QMainWindow::close);
}

void MainWindow::_createViewMenu()
{
	m_viewMenu->addAction("Show Normal");
	m_viewMenu->addAction("Show Minimized");
	m_viewMenu->addAction("Show Maximized");
	m_viewMenu->addAction("Show Fullscreen");
	m_viewMenu->addAction("Show text editor");

	QList<QAction*> actions = m_viewMenu->actions();
	actions[0]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_1));
	actions[1]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_2));
	actions[2]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_3));
	actions[3]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_4));

	QObject::connect(actions[0], &QAction::triggered, this, &QMainWindow::showNormal);
	QObject::connect(actions[1], &QAction::triggered, this, &QMainWindow::showMinimized);
	QObject::connect(actions[2], &QAction::triggered, this, &QMainWindow::showMaximized);
	QObject::connect(actions[3], &QAction::triggered, this, &QMainWindow::showFullScreen);
	QObject::connect(actions[4], &QAction::triggered, this, [this]() {
		TextEditor* editor = new TextEditor(this);
		editor->show();
	});
}

void MainWindow::_createHistoryMenu()
{
	m_historyMenu->addAction("Go forward");
	m_historyMenu->addAction("Go back");

	QList<QAction*> actions = m_historyMenu->actions();
	actions[0]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_F));
	actions[1]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_B));

	QObject::connect(actions[0], &QAction::triggered, m_tabWidget, &TabWidget::goForward);
	QObject::connect(actions[1], &QAction::triggered, m_tabWidget, &TabWidget::goBack);
}

void MainWindow::_createWindowMenu()
{
	m_windowMenu->addAction("Next tab");
	m_windowMenu->addAction("Previous tab");
	m_windowMenu->addAction("Reload page");
	QList<QAction*> actions = m_windowMenu->actions();
	actions[0]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_L));
	actions[1]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_K));
	actions[2]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_R));

	QObject::connect(actions[0], &QAction::triggered, m_tabWidget, &TabWidget::nextTab);
	QObject::connect(actions[1], &QAction::triggered, m_tabWidget, &TabWidget::previousTab);
	QObject::connect(actions[2], &QAction::triggered, m_tabWidget, &TabWidget::reloadCurrent);
}

void MainWindow::_createHelpMenu()
{
	m_helpMenu->addAction("Specifications");
	m_helpMenu->addSeparator();
	m_helpMenu->addAction("Documentation");
	m_helpMenu->addAction("About");

	QList<QAction*> actions = m_helpMenu->actions();
	actions[0]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_S));
	actions[2]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_D));
	actions[3]->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_A));

	QObject::connect(actions[0], &QAction::triggered, this, [this]() {
		SpecificationsDialog* specifications = new SpecificationsDialog(this);
		specifications->show();
	});

	QObject::connect(actions[2], &QAction::triggered, this, [this]() {
		DocumentationDialog* documentation = new DocumentationDialog(this);
		documentation->show();
		documentation->raise();
		documentation->activateWindow();
	});

	QObject::connect(actions[3], &QAction::triggered, this, [this]() {
		AboutDialog* about = new AboutDialog(this);

		QObject::connect(about, &AboutDialog::linkActivated, m_tabWidget, [this](const QString& link) {
			m_tabWidget->createTab();
			m_tabWidget->loadForCurrentTab(link);
		});

		about->show();
	});
}
