#include "aver_webengine.hpp"
#include <QContextMenuEvent>
#include <QMenu>

WebEngineView::WebEngineView(QWidget *parent)
	: QWebEngineView(parent),
	  m_menu(nullptr)
{
}

#ifndef QT_NO_CONTEXTMENU

void WebEngineView::contextMenuEvent(QContextMenuEvent* event)
{
	m_menu = page()->createStandardContextMenu();
	QList<QAction*> actions = m_menu->actions();
	m_menu->removeAction(actions.at(actions.count() - 1));
	m_menu->removeAction(actions.at(actions.count() - 1));
	m_menu->removeAction(actions.at(actions.count() - 1));
	m_menu->popup(event->globalPos());
}

#endif // QT_NO_CONTEXTMENU

WebEnginePage::WebEnginePage(QWidget* parent)
	: QWebEnginePage(parent)
{

}
